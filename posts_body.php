<?php

while ($LatestRow = mysqli_fetch_array($LatestSql)){
	
	$MediaActive = $LatestRow['active'];
	$MediaId = $LatestRow['id'];
	$MediaType = $LatestRow['type'];
	$MediaVotes = $LatestRow['votes'];
	$PostedById = $LatestRow['uid'];

	$longTitle = stripslashes($LatestRow['title']);
	$MediaTitle = short_title($longTitle);
	$longTitle = read_more_title($longTitle, $CountPost);
	
	$MediaLink = convertvn($MediaTitle);
	$MediaLink = nl2br($MediaLink);
	$MediaUrl = "post-".$MediaId."-".$MediaLink.".html";
	
	$TotalComments = $LatestRow['cmts'];
	

if($SubmiterLatest = $mysqli->query("SELECT * FROM users WHERE uid='$PostedById'")){

	$SubmiterRowLatest = mysqli_fetch_array($SubmiterLatest);

	$SubmiterUserLatest = $SubmiterRowLatest['username'];
	
	$SubmiterUserId = $SubmiterRowLatest['uid'];

	$SubmiterLinkLatest = convertvn($SubmiterUserLatest);

	$SubmiterLatest->close();

}else{

	?>
	<script>
		errorpage();
	</script>
	<?php
}

?>

<div class="post-box media-<?php echo $CountPost;?> post-<?php echo $MediaId;?>">
<header class="post-header">
	<div class="post-title"><h3 class="title-<?php echo $MediaId;?>" style="word-wrap:break-word;"><?php echo $longTitle;?></h3></div><!--post-title-->
	<div class="post-footer"><?php echo show_number($LatestRow['views']);?> lượt xem &bull; <?php echo get_time_ago(strtotime($LatestRow['date']));?> &bull; <a href="profile-<?php echo $PostedById;?>-<?php echo $SubmiterLinkLatest;?>.html"><span class="uname-color"><?php echo $SubmiterUserLatest;?></span></a></div>
</header>
<div class="post-left">

<?php 
if ($MediaActive < 0) { ?>

<div class="post-header">
	<div>Bài đăng có báo cáo vi phạm hoặc nội dung chưa hấp dẫn!</div>
	<div class="btn btn-danger btn-sm" data-id="<?php echo $MediaId;?>" onclick="deletepost(this)" style="margin: 5px 0px 5px auto; float: right;">Xóa</div>
</div>

<?php } else { ?>

<a href="<?php echo $MediaUrl; ?>" <?php if ($MediaOpen==1){?> target="_blank" <?php }?>>
<div class="post">

<?php if($MediaType=='1') { ?>

	<img class="img-responsive" src="<?php echo $Settings['datalink']; ?>/uploads/<?php echo $LatestRow['image'];?>" alt="" style="width: 600px;">

<?php } elseif ($MediaType=='2') { ?>

	<div style="position: relative;">
	<div class="gif-img"><span class="glyphicon glyphicon-picture"></span> GIF</div>
	<div class="frame-post-body"></div>
	<img class="img-responsive thumb" data-gifffer="<?php echo $Settings['datalink']; ?>/uploads/<?php echo $LatestRow['image'];?>" alt="" style="width: 600px;">
	</div>

<?php } elseif ($MediaType=='3') {
$Host = $LatestRow['video_type']; ?>

	<!-- youtube -->
	<?php if ($Host == "youtube") { ?>

		<div class="youtube-post img-responsive">
			<div class="play-button"><span class="glyphicon glyphicon-facetime-video"></span>&nbsp; Video</div>
			<img class="youtube-img" alt="Video" src="https://img.youtube.com/vi/<?php echo $LatestRow['video_id'];?>/hqdefault.jpg">
		</div>

	<?php } else if ($Host =="facebook") { ?>
		<div style="position: relative;">
			<div class="play-button"><span class="glyphicon glyphicon-facetime-video"></span>&nbsp; Video</div>
			<div class="frame-post-body"></div>
			<div class="fb-video" data-href="https://www.facebook.com/facebook/videos/<?php echo $LatestRow['video_id']; ?>/" data-show-text="false" data-controls="false" data-width="600" data-height="350" data-allowfullscreen="true"></div>
		</div>
	<?php }
} ?>
</div>
</a>

<div class="vote-post">

<div class="vote-box vote-<?php echo $MediaId;?>">

<?php if(isset($_SESSION['useremail'])){?>
	<!-- Vote up -->
	<i <?php if (userLiked($MediaId)): ?> class="btn fas fa-thumbs-up"
	   <?php else: ?> class="btn far fa-thumbs-up" <?php endif; ?>
	   data-id="<?php echo $MediaId;?>" onclick="voteup(this)"></i>
	<!-- Show points -->
	<span class="display-vote" data-points="<?php echo $MediaVotes;?>"><?php echo show_number($MediaVotes); ?></span>
	<!-- Vote down -->
	<i <?php if (userDisliked($MediaId)): ?> class="btn fas fa-thumbs-down"
	   <?php else: ?> class="btn far fa-thumbs-down" <?php endif; ?>
	   data-id="<?php echo $MediaId;?>" onclick="votedown(this)"></i>
<?php } else { ?>
	<!-- Vote up -->
	<i class="btn far fa-thumbs-up" data-toggle="modal" data-target="#myModal" title="Vui lòng đăng nhập!"></i>
	<!-- Show points -->
	<span class="display-vote"><?php echo show_number($MediaVotes); ?></span>
	<!-- Vote down -->
	<i class="btn far fa-thumbs-down" data-toggle="modal" data-target="#myModal" title="Vui lòng đăng nhập!"></i>
<?php } ?>

</div><!--vote-box-->


<div class="social-box">

  <?php if(isset($_SESSION['useremail'])){?>
	<a href="<?php echo $MediaUrl; ?>#binh-luan-<?php echo $MediaId;?>" class="btn far fa-comment-alt"></a>
  <?php } else { ?>
	<i class="btn far fa-comment-alt" data-toggle="modal" data-target="#myModal" title="Vui lòng đăng nhập!"></i>
  <?php } ?>
	<span class="display-vote" data-id="<?php echo $MediaId;?>"><?php echo show_number($TotalComments); ?></span>
</div><!--social-box-->


<div class="dropup pull-right other-btn">

  	<i class="btn fas fa-ellipsis-h" role="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>

	<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">

<?php if($SubmiterUserId == $Uid){?>
	<!-- 1 Edit button -->
	<li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(0)" data-id="<?php echo $MediaId;?>" onclick="editpost(this)"><i class="far fa-edit" style="font-size: 16px;"></i>&nbsp; Sửa bài đăng</a></li>
		<li class="divider"></li>
	<!-- 1 Delete button -->
	<li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(0)" data-id="<?php echo $MediaId;?>" onclick="deletepost(this)"><i class="far fa-trash-alt" style="font-size: 20px;"></i>&nbsp; Xóa bài đăng</a></li>

<?php } else { ?>
  
	<?php if(isset($_SESSION['useremail'])){?>
		<!-- 1 Report button -->
		<?php if (userReported($MediaId)): ?>
			<li role="presentation" class="disabled"><a><i class="fas fa-shield-virus" style="font-size: 18px;"></i>&nbsp; Đã báo cáo!</a></li>
		<?php else: ?>
    		<li class="reportbtn-<?php echo $MediaId;?>" role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(0)" data-id="<?php echo $MediaId;?>" onclick="reportpost(this)"><i class="fas fa-shield-virus" style="font-size: 18px;"></i>&nbsp; Báo cáo vi phạm</a></li>
		<?php endif; ?>

	<?php } else { ?>
		<!-- 1 Go to login -->
		<li class="reportbtn-<?php echo $MediaId;?>" role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(0)" data-toggle="modal" data-target="#myModal" title="Vui lòng đăng nhập!"><i class="fas fa-shield-virus" style="font-size: 18px;"></i>&nbsp; Báo cáo vi phạm</a></li>

	<?php } ?>
		<li class="divider"></li>
		<!-- 2 Hide post button -->
		<li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(0)" data-id="<?php echo $CountPost;?>" onclick="hidepost(this)"><i class="fas fa-ban" style="font-size: 18px;"></i>&nbsp; Ẩn bài đăng</a></li>

<?php } ?>

		<li class="divider"></li>
	<!-- 3 Facebook share button -->
	<li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(0);" onclick="popup('http://www.facebook.com/share.php?u=https://vuikk.com/<?php echo $MediaUrl; ?>&amp;title=<?php echo stripslashes($LatestRow['title']); ?>')"><img src="<?php echo $Settings['datalink']; ?>/sysimg/facebook-btn.svg" height="18">&nbsp; Chia sẻ Facebook</a></li>
		<li class="divider"></li>
	<!-- 4 Zalo share button -->
	<li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(0)" class="zalo-share-button" data-href="https://vuikk.com/<?php echo $MediaUrl; ?>" data-oaid="579745863508352884" data-customize=true><img src="<?php echo $Settings['datalink']; ?>/sysimg/zalo-btn.svg" height="18">&nbsp; Chia sẻ Zalo</a></li>
	</ul>

</div>

		</div><!--vote-post-->

<?php } ?>		

	</div><!--post-left-->

</div><!--post-box-->

<?php
$CountPost++;

//$Ads every 4 posts
if($CountPost % 4 == 0)
{
	if(!empty($Ad5)){?> 
		<div class="post-box">
			<div class="ads-five" id="ads-<?php echo $CountPost; ?>">
				<?php echo $Ad5; ?>
			</div>
		</div><!--ad--> 
		<?php
	}
}

//end while
} ?>