<?php session_start();
session_destroy();

// clear cookies
clearCookie();

header("Location: .");

function clearCookie() {
    if (isset($_COOKIE["useremail"])) {
        setcookie("useremail", "");
    }
    if (isset($_COOKIE["snid"])) {
        setcookie("snid", "");
    }
    if (isset($_COOKIE["password"])) {
        setcookie("password", "");
    }
}

?>