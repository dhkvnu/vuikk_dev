<?php session_start();
include('../db.php');
include('convertvn.php');

if($SettingsSql = $mysqli->query("SELECT * FROM settings WHERE id='1'")){

    $Settings = mysqli_fetch_array($SettingsSql);
	
	$Active = $Settings['active'];

	$LimitPost = $Settings['limit_posts'];
	
	$SettingsSql->close();
	
}else{
    
	?>
	<script>
		errorpage();
	</script>
	<?php
}

//Get user info

$uEmail = $_SESSION['useremail'];

if($UserSql = $mysqli->query("SELECT * FROM users WHERE email='$uEmail'")){

  $UserRow = mysqli_fetch_array($UserSql);

	$Uname  = $UserRow['username'];

	$Uid = $UserRow['uid'];
	
  $UserSql->close();
	
}else{
     
	?>
	<script>
		errorpage();
	</script>
	<?php
	 
}


//Validation

if($_POST)
{		
	//Check url
	$UrlCheck = $_POST['mLink'];

	if(!isset($_POST['mLink']) || strlen($_POST['mLink'])<1)
	{
		//required variables are empty
		//die('<div class="alert alert-danger">Please add a video source URL.</div>');
		die('<div class="alert alert-danger">Vui lòng dán liên kết video!</div>');
	}

	if (preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i", $UrlCheck)) {
		//do nothing
  	}else {	
	  die('<div class="alert alert-danger">Liên kết sai định dạng.</div>');
	}
	  	
	//Check video and get $VideoId
	$check1 = substr($UrlCheck,0,17);
	$check2 = substr($UrlCheck,0,24);
	$check3 = substr($UrlCheck,0,25);

	if($check1 == "https://youtu.be/" || $check2 == "https://www.youtube.com/"){
		$VideoType = "youtube";
		$VideoId   = get_youtube_videoid($UrlCheck);
	} 
	elseif($check3 == "https://www.facebook.com/"){
		$VideoType = "facebook";
		$VideoId   = get_facebook_videoid($UrlCheck);
	}
	else {
		die('<div class="alert alert-danger">Nguồn video không được hỗ trợ.</div>');
	}


	if(!isset($_POST['catagory-select-2']) || strlen($_POST['catagory-select-2'])<1)
	{
		//required variables are empty
		die('<div class="alert alert-danger">Vui lòng chọn chủ đề!</div>');
	}
	
	if(!isset($_POST['vName']) || strlen($_POST['vName'])<1)
	{
		//required variables are empty
		die('<div class="alert alert-danger">Vui lòng nhập tiêu đề!</div>');
	}
	
		
	$FileTitle			= mb_ucfirst($mysqli->escape_string(nl2br(htmlspecialchars($_POST['vName'])))); // file title
	$VideoId            = $mysqli->escape_string($VideoId);
	$Catagory           = $mysqli->escape_string($_POST['catagory-select-2']);
	$Date               = (new DateTime('now', new DateTimeZone('Asia/Ho_Chi_Minh')))->format('c');
	$Type				= "3";
	
	if(!empty($VideoType)){

		$mysqli->query("INSERT INTO media(title, video_type, video_id, type, catid, uid, date, active) VALUES ('$FileTitle','$VideoType','$VideoId','$Type','$Catagory','$Uid','$Date','$Active')");
		
		if (checkNumPosts() == -1) {
			$UpdateBonus = $mysqli->query("UPDATE users SET bonus=bonus-1 WHERE uid='$Uid' AND bonus>0");
		
			$Maxpost = checkbonus();
		} else {
			$Maxpost = checkNumPosts() + checkbonus();
		}
	}
?>

<script>

function removeModel() {
$('#modelVideos').modal('hide');
$('body').removeClass('modal-open');
$('.modal-backdrop').remove();
$("#output-video").empty();
$('#VideoSubmit').resetForm();
$('#vName').height(0);

var datapost = "<?php echo $Maxpost; ?>";
$('#postimage').attr('data-post', datapost);
$('#postvideo').attr('data-post', datapost);

var maxpost = "<?php echo number_format($Maxpost, 0, ',', '.'); ?>";
$('#maxpost').html(maxpost);
}

setTimeout(removeModel,1500);

</script>


<?php
		
		die('<div class="alert alert-info" style="text-align: center;">Cảm ơn bạn đã đăng bài!</div>');
		   
}else{
   	die('<div class="alert alert-danger">Đã xảy ra sự cố. Vui lòng thử lại!</div>');
}


//Get youtube video id
function get_youtube_videoid($videourl) {
	preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $videourl, $vid);
	return $vid[1];
}

function get_facebook_videoid($videourl) {
	preg_match('/videos\/(\d+)+|v=(\d+)|vb.\d+\/(\d+)/',  $videourl, $vid);
	return $vid[1]; 
}


// Check if number post limited
function checkNumPosts() {
	global $mysqli;
	global $Uid, $LimitPost;
	$BeginToday = strtotime((new DateTime('today', new DateTimeZone('Asia/Ho_Chi_Minh')))->format('c'));
	$MediaSql = "SELECT id FROM media WHERE uid='$Uid' AND UNIX_TIMESTAMP(date)>$BeginToday";
	$MediaResult = mysqli_query($mysqli, $MediaSql);
	$MediaCount = mysqli_num_rows($MediaResult);
	if ($MediaCount > $LimitPost) {
	  $numPost = -1;
	} else {
	  $numPost = $LimitPost - $MediaCount;
	}
	return $numPost;
}


function checkbonus() {
	global $mysqli;
	global $Uid;
	$MediaSql = "SELECT * FROM users WHERE uid='$Uid' limit 1";
	$MediaResult = mysqli_query($mysqli, $MediaSql);
	$MediaRow = mysqli_fetch_array($MediaResult);
	$Ubonus = $MediaRow['bonus'];
	return $Ubonus;
}

?>