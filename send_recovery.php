<?php
include('../db.php');

if($_POST)
{	
	if(!isset($_POST['inputRecovery']) || strlen($_POST['inputRecovery'])<1)
	{
		//required variables are empty
		die('<div class="alert alert-danger" role="alert">Vui lòng nhập địa chỉ email!</div');
	}
	
	$uEmail = $_POST['inputRecovery'];
	
	if (filter_var($uEmail, FILTER_VALIDATE_EMAIL)) {
  	// The email address is valid
	} else {
  		die('<div class="alert alert-danger" role="alert">Email không hợp lệ.</div>');
	}
		
if($SettingsSql = $mysqli->query("SELECT * FROM settings WHERE id='1'")){

    $Settings = mysqli_fetch_array($SettingsSql);
	
		$MFROM 	= stripslashes($Settings['sendemail']);
	  
		$MPASS 	= stripslashes($Settings['sendpass']);
	
	$SettingsSql->close();
	
}else{
	?><script>errorpage();</script><?php
}

//Get User Details

if($UserCheck = $mysqli->query("SELECT * FROM users WHERE email='$uEmail' AND snid ='' AND u_active ='1' limit 1")){

   	$GetUserInfo = mysqli_fetch_array($UserCheck);
	
	$UserCount= mysqli_num_rows($UserCheck);

   	$UserCheck->close();
   
	}else{
   
   		?><script>errorpage();</script><?php

}

if ($UserCount==1){

$UserName 			 = $GetUserInfo['username'];
$UserId				 = $GetUserInfo['uid'];	
$ToContact	 	 	 = $GetUserInfo['email'];;

$FromName			 = $Settings['name'];
$FromEmail			 = $MFROM;
$FromSubject		 = "Cài đặt lại mật khẩu";
$SiteURL			 = $Settings['siteurl'];


function rand_string( $length ) {

$chars = "abcdefghijklmnopqrstuvwxyz0123456789*#@!";
return substr(str_shuffle($chars),0,$length);

}
$NewPassword = rand_string(8);
$EncryptPassword = md5($NewPassword);

$FromMessage = 
"Xin chào <b>" . $UserName . "</b>,
<br/><br/>
Mật khẩu của bạn đã được cài đặt lại.
<br/><br/>
Email đăng nhập: ".$uEmail."
<br/>
<br/>
Mật khẩu mới:  <b>".$NewPassword."</b>
<br/><br/>

Để thay đổi mật khẩu, vui lòng đăng nhập bằng mật khẩu mới,
sau đó vào phần \"Cài đặt\", chọn \"Thay đổi mật khẩu.\"
<br/>
<br/>
Chân thành cảm ơn!
<br/>
<a href=" . $SiteURL . ">" . $FromName . "</a>";

//Setup smtp
require_once('include/class.phpmailer.php');
$mail             = new PHPMailer();

$mail->IsSMTP();             
$mail->CharSet  = "utf-8";
$mail->SMTPDebug  = 0;   // enables SMTP debug information (for testing)
$mail->SMTPAuth   = true;    // enable SMTP authentication
$mail->SMTPSecure = "ssl";   // sets the prefix to the servier
$mail->Host       = "smtp.gmail.com";    // sever gui mail.
$mail->Port       = 465;         // port gui mail de nguyen
// Login smtp
$mail->Username   = $MFROM;  // khai bao dia chi email
$mail->Password   = $MPASS;  // khai bao mat khau

//Send from
$mail->SetFrom($FromEmail, $FromName);
//mail to
$mail->AddAddress($ToContact);
//subject
$mail->Subject = $FromSubject;
//body
$mail->MsgHTML($FromMessage);
//reply to
$mail->AddReplyTo($FromEmail, $FromName);

if(!$mail->Send()) {

?>

<div class="alert alert-danger" role="alert">Đã xảy ra lỗi gửi email!</div>

<?php 

	} else {

	$mysqli->query("UPDATE users SET password='$EncryptPassword' WHERE uid='$UserId'");	
	
?>

	<div class="alert alert-success" role="alert">Mật khẩu của bạn đã được cài đặt lại! Vui lòng kiểm tra mật khẩu mới đã gửi tới email!</div>

<?php }

	} else{ ?>
	
	<div class="alert alert-danger" role="alert">Email không tồn tại. Vui lòng nhập địa chỉ email khác!</div>
	
<?php }


}


?>