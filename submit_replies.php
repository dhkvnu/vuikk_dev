<?php

// Error reporting:
error_reporting(E_ALL^E_NOTICE);

include('../db.php');
include('convertvn.php');
//Comments Data

class Comment
{

	private $data = array();
	
	public function __construct($row)
	{
		/*
		/	The constructor
		*/
		
		$this->data = $row;
	}
	
	public function markup($lastId)
	{
		/*
		/	This method outputs the XHTML markup of the comment
		*/
		
		// Setting up an alias, so we don't have to write $this->data every time:
		$d = &$this->data;

		$UserUrl = "profile-".$d['ruidr']."-".convertvn($d['namer']).".html";
				
		return '
		
			<div class="replies-box" style="margin-left: 5px; margin-right: -5px;">

					<a href="'.$UserUrl.'">
						<img style="width: 20px; height: 20px; border-radius: 50%; object-fit: cover;" src='.$d['avatarlinkr'].' class="media-object pull-left">
					</a>
					
					<div class="media-replies-body" id="comment-body-'.$lastId.'">
						<h4 class="media-heading" style="font-size: 12px;"><a href="'.$UserUrl.'">'.$d['namer'].'</a><span style="color: #999; font-weight: normal;">&nbsp;Vừa đăng<span id="cmtEdited-'.$lastId.'"></span></span></h4>
						<h5 style="word-wrap:break-word;"><b style="color: #337AB7;">'.$d['tousername'].'</b>&nbsp;<span id="cmtContent-'.$lastId.'">'.$d['replycomment'].'</span></h5>

						<div class="comment-vote-box">
							<i class="btn far fa-thumbs-up" data-id="'.$lastId.'" onclick="voteup_comment(this)" style="font-size: 14px;"></i>			
							<span class="display-vote-comment" data-points="0">0</span>
							<i class="btn far fa-thumbs-down" data-id="'.$lastId.'" onclick="votedown_comment(this)" style="font-size: 14px;"></i>

							<a href="javascript:void(0)" id="editcmt_content-'.$lastId.'" data-id="'.$lastId.'" data-content="'.str_replace("<br />", "\n", $d['replycomment']).'" onclick="edit_comment(this)" style="color: #065FD4;">Sửa</a>
						</div>

					</div>			

			</div>
		';
	}
	
	public static function validate(&$arr)
	{
		/*
		/	This method is used to validate the data sent via AJAX.
		/
		/	It return true/false depending on whether the data is valid, and populates
		/	the $arr array passed as a paremter (notice the ampersand above) with
		/	either the valid input data, or the error messages.
		*/
		
		$errors = array();
		$data	= array();
		
		// Using the filter with a custom callback function:
		
		if(!($data['replycomment'] = filter_input(INPUT_POST,'replycomment',FILTER_CALLBACK,array('options'=>'Comment::validate_text'))))
		{
			$errors['replycomment'] = 'Sai định dạng.';
		}
		
		if(!($data['namer'] = filter_input(INPUT_POST,'namer',FILTER_CALLBACK,array('options'=>'Comment::validate_text'))))
		{
			$errors['namer'] = 'Đã xảy ra lỗi.';
		}
		
		if(!($data['ruidr'] = filter_input(INPUT_POST,'ruidr',FILTER_CALLBACK,array('options'=>'Comment::validate_text'))))
		{
			$errors['ruidr'] = 'Đã xảy ra lỗi.';
		}
		
		if(!($data['pidr'] = filter_input(INPUT_POST,'pidr',FILTER_CALLBACK,array('options'=>'Comment::validate_text'))))
		{
			$errors['pidr'] = 'Đã xảy ra lỗi.';
		}

		if(!($data['parentid'] = filter_input(INPUT_POST,'parentid',FILTER_CALLBACK,array('options'=>'Comment::validate_text'))))
		{
			$errors['parentid'] = 'Đã xảy ra lỗi.';
		}

		if(!($data['touserid'] = filter_input(INPUT_POST,'touserid',FILTER_CALLBACK,array('options'=>'Comment::validate_text'))))
		{
			$errors['touserid'] = 'Đã xảy ra lỗi.';
		}

		if(!($data['tousername'] = filter_input(INPUT_POST,'tousername',FILTER_CALLBACK,array('options'=>'Comment::validate_text'))))
		{
			$errors['tousername'] = 'Đã xảy ra lỗi.';
		}
		
		if(!($data['avatarlinkr'] = filter_input(INPUT_POST,'avatarlinkr',FILTER_CALLBACK,array('options'=>'Comment::validate_text'))))
		{
			$errors['avatarlinkr'] = 'Đã xảy ra lỗi.';
		}
		
		if(!empty($errors)){
			
			// If there are errors, copy the $errors array to $arr:
			
			$arr = $errors;
			return false;
		}
		
		// If the data is valid, sanitize all the data and copy it to $arr:
		
		foreach($data as $k=>$v){
			$arr[$k] = ($v);
		}
				
		return true;
		
	}

	private static function validate_text($str)
	{
		/*
		/	This method is used internally as a FILTER_CALLBACK
		*/
		
		if(mb_strlen($str,'utf8')<1)
			return false;
		
		// Encode all html special characters (<, >, ", & .. etc) and convert
		// the new line characters to <br> tags:
		
		$str = nl2br(htmlspecialchars($str));
		
		// Remove the new line characters that are left
		$str = str_replace(array(chr(10),chr(13)),'',$str);
		
		return $str;
	}

}



/*
/	This array is going to be populated with either
/	the data that was sent to the script, or the
/	error messages.
/*/

$Date		        = (new DateTime('now', new DateTimeZone('Asia/Ho_Chi_Minh')))->format('c');

$arr = array();
$validates = Comment::validate($arr);

if($validates)
{

	$ParentId = $mysqli->escape_string($arr['parentid']);
	$CheckParent = $mysqli->query("SELECT id FROM comments WHERE id='$ParentId'");
	$CountParent = mysqli_num_rows($CheckParent);
	$CheckParent->close();

	if($CountParent != 0) {
	/* Everything is OK, insert to database: */
	
	$CommentsTxt = $mysqli->escape_string($arr['replycomment']);
	
	if($mysqli->query("INSERT INTO comments(comment,date,uid,pid,parent,to_uid) VALUES ('".$CommentsTxt."','".$Date."','".$arr['ruidr']."','".$arr['pidr']."','".$arr['parentid']."','".$arr['touserid']."')")){
		$lastrepId = $mysqli->insert_id;
	}
	
	$mysqli->query("UPDATE comments, media SET comments.repnum=comments.repnum+1, media.cmts=media.cmts+1 WHERE comments.id='".$arr['parentid']."' AND media.id='".$arr['pidr']."'");
	
	} else {
		$lastrepId = uniqid();
	}
	
	$arr['date'] = date('r',time());
	
	$arr = array_map('stripslashes',$arr);
	
	$insertedComment = new Comment($arr);

	echo json_encode(array('status'=>1,'html'=>$insertedComment->markup($lastrepId)));

}
else
{
	/* Outputtng the error messages */
	echo '{"status":0,"errors":'.json_encode($arr).'}';
}

?>