<?php include("header.php");?>

<div class="container">

<div class="col-md-8" id="left">

<?php
$term = $mysqli->escape_string($_GET['term']);
$page = $_GET["page"];
$start = ($page - 1) * 8;
$CountPost = $start;

if($LatestSql = $mysqli->query("SELECT * FROM media WHERE (title LIKE '%$term%') AND active=1 ORDER BY id DESC LIMIT $start, 8")){

while ($LatestRow = mysqli_fetch_array($LatestSql)){
	
	$MediaId = $LatestRow['id'];
	$MediaType = $LatestRow['type'];
	$MediaVotes = $LatestRow['votes'];
		
	$longTitle = stripslashes($LatestRow['title']);
	$MediaTitle = short_title($longTitle);
	
	$strSidebar = strlen($longTitle);
	if ($strSidebar > 72) {
	$longTitle = substr($longTitle,0,72).'...';
	}
	
	$MediaLink = convertvn($MediaTitle);
	$MediaLink = nl2br($MediaLink);
	
	$TotalComments = $LatestRow['cmts'];
	
?>

<div class="media-box">
	<a class="pull-left" href="post-<?php echo $MediaId;?>-<?php echo $MediaLink;?>.html" <?php if ($MediaOpen==1){?> target="_blank" <?php }?>>

<?php if($MediaType=='1'){?>

	<img class="media-object" alt="<?php echo $LatestRow['image'];?>" src="<?php echo $Settings['datalink']; ?>/uploads/<?php echo $LatestRow['image'];?>" style="width: 220px; height: 120px; object-fit: cover;">

<?php } elseif($MediaType=='2'){ ?>

<div style="width: 220px; height: 120px; overflow: hidden;">

	<div class="gif-img"><span class="glyphicon glyphicon-picture"></span> GIF</div>
	<img class="media-object" alt="" data-gifffer="<?php echo $Settings['datalink']; ?>/uploads/<?php echo $LatestRow['image'];?>" style="width: 220px;">

</div>
	
<?php } elseif ($MediaType=='3'){?>

	<?php if ($LatestRow['video_type'] == "youtube") { ?>

	<div class="play-button"><span class="glyphicon glyphicon-facetime-video"></span>&nbsp; Video</div>
	<img class="media-object" alt="" src="https://img.youtube.com/vi/<?php echo $LatestRow['video_id'];?>/hqdefault.jpg" style="width: 220px; height: 120px; object-fit: cover;">

	<?php } elseif ($LatestRow['video_type'] =="facebook") { ?>
	<div style="width: 220px; height: 120px;">
	<div class="play-button"><span class="glyphicon glyphicon-facetime-video"></span>&nbsp; Video</div>
	<div class="video-frame-post"></div>
	<div class="fb-video" data-href="https://www.facebook.com/facebook/videos/<?php echo $LatestRow['video_id']; ?>/" data-show-text="false" data-width="220" data-height="120" data-controls="false"></div>
	</div>
	<?php } ?>
<?php }?>
</a>
  		<div class="media-body-box">
    		<a href="post-<?php echo $MediaId;?>-<?php echo $MediaLink;?>.html" <?php if ($MediaOpen==1){?> target="_blank" <?php }?>><h4 class="media-heading" style="word-wrap:break-word;"><?php echo stripslashes($LatestRow['title']);?></h4></a>
		  <p><?php echo show_number($LatestRow['views']);?> lượt xem</p>
          <p><?php echo show_number($TotalComments);?> bình luận</p>
          <p><?php echo show_number($MediaVotes); ?> thích</p>
       </div>
    </div>
  </div>

<?php     
	}
$LatestSql->close();
}else{
    ?>
	<script>
		errorpage();
	</script>
	<?php
}
?>

<nav id="page-nav"><a href="data_search.php?term=<?php echo $term;?>&amp;page=2"></a></nav>

<script src="js/jquery.infinitescroll.min.js"></script>
	<script src="js/manual-trigger.js"></script>
	
	<script>
	
	
	$('#left').infinitescroll({
		navSelector  : '#page-nav',    // selector for the paged navigation 
      	nextSelector : '#page-nav a',  // selector for the NEXT link (to page 2)
      	itemSelector : '.media-box',     //
		loading: {
          				finishedMsg: 'Đã tải xong',
          				img: '<?php echo $Settings['datalink']; ?>/sysimg/ajax-loader.gif'
        			}
		
		
    },
	function(){

		$.getScript('js/scroll_load.js');

	},
	function(newElements, data, url){
		//Gif Code
		$.getScript('js/ready_functions.js');
    });
</script>


</div><!--/.col-md-8 -->



</div><!--/.container-->
<br>
<?php include("footer.php");?>

