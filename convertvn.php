<?php
//Function to convert Vietnamese
function convertvn($str) {
    $str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", "a", $str);   //step 1
    $str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", "e", $str);
    $str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", "i", $str);
    $str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", "o", $str);
    $str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", "u", $str);
    $str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", "y", $str);
    $str = preg_replace("/(đ)/", "d", $str);
    $str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", "A", $str);
    $str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", "E", $str);
    $str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", "I", $str);
    $str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", "O", $str);
    $str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", "U", $str);
    $str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", "Y", $str);
    $str = preg_replace("/(Đ)/", "D", $str);
    $str = preg_replace("![^a-z0-9]+!i", "-", $str);   //step 2
    $str = urlencode($str);  // step 3
    $str = strtolower($str);  //step 4
    return $str;
  }

/**
 * ucfirst() function for multibyte character encodings
 *
 * @param $string
 * @param string $encoding
 * @return string
 */
function mb_ucfirst($string, $encoding = 'utf-8')
{
    if (empty($string) || !is_string($string)) {
        return null;
    }
    
    $strLen = mb_strlen($string, $encoding);
    $firstChar = mb_substr($string, 0, 1, $encoding);
    $then = mb_substr($string, 1, $strLen - 1, $encoding);
    
    return mb_strtoupper($firstChar, $encoding) . $then;
}


//Function to get time ago
function get_time_ago( $time )
{
    $time_difference = time() - $time;

    if( $time_difference < 60 ) { return 'Vừa đăng'; }
    $condition = array( 12 * 30 * 24 * 60 * 60 =>  'năm',
                30 * 24 * 60 * 60       =>  'tháng',
                6.5 * 24 * 60 * 60      =>  'tuần',
                23.5 * 60 * 60          =>  'ngày',
                59.5 * 60               =>  'giờ',
                60                      =>  'phút',
    );

    foreach( $condition as $secs => $str )
    {
        $d = $time_difference / $secs;

        if( $d >= 1 )
        {
            $t = round( $d );
            return $t . ' ' . $str . ' trước';
        }
    }
}


//Function to convert Vote Points
function show_number($input)
{
    if ( is_numeric($input) ) {
    $suffixes = array('', 'N', 'Tr', 'T', 'NT');
    $suffixIndex = 0;

    while(abs($input) >= 1000 && $suffixIndex < sizeof($suffixes))
    {
        $suffixIndex++;
        $input /= 1000;
        $input = round($input,1);
    }

    return str_replace('.',',', (
        $input > 0
            // precision of 3 decimal places
            ? floor($input * 1000) / 1000
            : ceil($input * 1000) / 1000
        ))
        . $suffixes[$suffixIndex];
    } else {
        return "-";
    }
}


function short_title($str) {
	if (strlen($str) > 45) {
	$newstr = substr($str,0,45).'...';
	} else {
	$newstr = $str;
    }
    return $newstr;
}


function read_more_title($str, $id) {
	if (strlen($str) > 150) {
  		$newstr = substr($str,0,150);
  		$newstr .= '<span id="view-more-'.$id.'" data-id="'.$id.'" onclick="showtext(this)">... <span style="color: #757575; cursor: pointer;">Xem thêm</span></span><span class="more-text" id="more-text-'.$id.'">';
  		$newstr .= substr($str,150,strlen($str));
  		$newstr .= '</span>';
	} else {
        $newstr = $str;
    }
    return $newstr;
}

?>