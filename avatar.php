<?php session_start();
include('../db.php');
include('convertvn.php');

if($SettingsSql = $mysqli->query("SELECT * FROM settings WHERE id='1'")){

    $Settings = mysqli_fetch_array($SettingsSql);
	$SettingsSql->close();
	
}else{
	?><script>errorpage();</script><?php
}

//Get user info

$uEmail = $_SESSION['useremail'];

if($UserSql = $mysqli->query("SELECT * FROM users WHERE email='$uEmail'")){

  $UserRow = mysqli_fetch_array($UserSql);

	$Uid = $UserRow['uid'];

	$avatrimage = $UserRow['avatar'];
	
  $UserSql->close();
	
}else{
     
	?><script>errorpage();</script><?php
	 
}


//Data lake
require_once 'datalake/vendor/autoload.php';
use MicrosoftAzure\Storage\Blob\BlobRestProxy;
use MicrosoftAzure\Storage\Common\Exceptions\ServiceException;
use MicrosoftAzure\Storage\Blob\Models\CreateBlockBlobOptions;
//Access API
$ACCOUNT_NAME      = $Settings['dataname'];
$ACCOUNT_KEY       = $Settings['data_key'];
$connectionString  = "DefaultEndpointsProtocol=https;AccountName=".$ACCOUNT_NAME.";AccountKey=".$ACCOUNT_KEY;
$blobClient        = BlobRestProxy::createBlobService($connectionString); // Create blob client.
$containerName     = "data/avatars"; //Upload container
$options           = new CreateBlockBlobOptions();   //File type


if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST")
{

	if(!isset($_FILES['inputfile']))
	{
		//required variables are empty
		echo json_encode(array('msg'=>"<div class='alert alert-danger'>Vui lòng chọn ảnh!</div>"));
		return;
	}


	//File property
	$file 					= $_FILES['inputfile']['tmp_name']; 
    $sourceProperties		= getimagesize($file);     
	list($width, $height) 	= $sourceProperties;
	$imageType 				= $sourceProperties[2];
	
	
	switch ($imageType) {
		//allowed file types
		case IMAGETYPE_JPEG:
			$Type = "1";
			if($width > 600){
				$imageResourceId = imagecreatefromjpeg($file);
				$targetLayer = imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
				ob_start(); // start a new output buffer
  					imagejpeg($targetLayer);
					$imagedata = base64_encode(ob_get_clean());
				$content = fopen("data:image/jpg;base64,$imagedata", "r"); 	//data lake file content
			}else{
				$content = fopen($file, "r"); //data lake file content
			}
			$options->setContentType("image/jpg"); //data lake file type
			$ext = "jpg";
			break;

		case IMAGETYPE_PNG:
			$Type = "1";
			if($width > 600){
				$imageResourceId = imagecreatefrompng($file);
				$targetLayer = imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
				ob_start(); // start a new output buffer
					imagejpeg($targetLayer);
					$imagedata = base64_encode(ob_get_clean());
				$content = fopen("data:image/jpg;base64,$imagedata", "r"); 	//data lake file content
			}else{
				$content = fopen($file, "r"); //data lake file content
			}
			$options->setContentType("image/jpg"); //data lake file type
			$ext = "jpg";
			break;

		case IMAGETYPE_GIF:
			$Type = "2";
			if( filesize($file) > 3 * 1048576 ){
				echo json_encode(array('msg'=>"<div class='alert alert-danger'>Vui lòng chọn ảnh động kích thước nhỏ hơn 3M.</div>"));
				return;
			}else{
				$content = fopen($file, "r");
			}
			$options->setContentType("image/gif"); //data lake file type
			$ext = "gif";
			break;
		
		default:
			echo json_encode(array('msg'=>"<div class='alert alert-danger'>Chọn sai định dạng. Vui lòng chọn lại ảnh!</div>"));//output error
			return;
			break;
	}
	
   //Rename and save uploded file to destination folder.
   if($Type == 1 || $Type == 2)
   {
	//Name catagory and post date
	$NewFileName	= $mysqli->escape_string(date("Ymd")."_".uniqid()."_".$Uid.".".$ext);
	
	try {
		//Upload blob to data lake
		$blobClient->createBlockBlob($containerName, $NewFileName, $content, $options);

		if(!empty($avatrimage) && substr($avatrimage,0,4) != "http"){
			// Delete old avatar
			$blobClient->deleteBlob($containerName, $avatrimage);
		}
	}
	catch(ServiceException $e){
		$code = $e->getCode();
		$error_message = $e->getMessage();
	}
	
	// Insert info into database
	$mysqli->query("UPDATE users SET avatar='$NewFileName' WHERE uid='$Uid'");

	// Free up memory
	unset($targetLayer);
	unset($imageResourceId);
	unset($imagedata);
	unset($content);

	echo json_encode(array('img'=>"<img src='".$Settings['datalink']."/avatars/".$NewFileName."' style='object-fit: cover;' />",'msg'=>"<div class='alert alert-info'>Đã đổi xong</div>"));
	return;
		
	} else{
		echo json_encode(array('msg'=>"<div class='alert alert-danger'>Đã xảy ra sự cố. Vui lòng thử lại!</div>"));
		return;
	}
}

//Function to resize image
function imageResize($imageResourceId,$width,$height) {

    $targetWidth = 200;
    $targetHeight = round($targetWidth * $height / $width);

    $targetLayer=imagecreatetruecolor($targetWidth,$targetHeight);
    imagecopyresampled($targetLayer,$imageResourceId,0,0,0,0,$targetWidth,$targetHeight, $width,$height);

	return $targetLayer;   
}

?>