<?php session_start();
include('../db.php');
include('convertvn.php');

if($SettingsSql = $mysqli->query("SELECT * FROM settings WHERE id='1'")){

    $Settings = mysqli_fetch_array($SettingsSql);
	
	$Active = $Settings['active'];

	$LimitPost = $Settings['limit_posts'];
	
	$SettingsSql->close();
	
}else{
	?><script>errorpage();</script><?php
}

//Get user info

$uEmail = $_SESSION['useremail'];

if($UserSql = $mysqli->query("SELECT * FROM users WHERE email='$uEmail'")){

  $UserRow = mysqli_fetch_array($UserSql);

	$Uname  = $UserRow['username'];

	$Uid = $UserRow['uid'];
	
  $UserSql->close();
	
}else{
     
	?><script>errorpage();</script>
	<?php
	 
}


//Data lake
require_once 'datalake/vendor/autoload.php';
use MicrosoftAzure\Storage\Blob\BlobRestProxy;
use MicrosoftAzure\Storage\Common\Exceptions\ServiceException;
use MicrosoftAzure\Storage\Blob\Models\CreateBlockBlobOptions;
//Access API
$ACCOUNT_NAME      = $Settings['dataname'];
$ACCOUNT_KEY       = $Settings['data_key'];
$connectionString  = "DefaultEndpointsProtocol=https;AccountName=".$ACCOUNT_NAME.";AccountKey=".$ACCOUNT_KEY;
$blobClient        = BlobRestProxy::createBlobService($connectionString); // Create blob client.
$containerName     = "data/uploads"; //Upload container
$options           = new CreateBlockBlobOptions();   //File type


if($_POST)
{	
	if(!isset($_FILES['mFile']))
	{
		//required variables are empty
		//die('<div class="alert alert-danger">Please select a image file</div>');
		die('<div class="alert alert-danger">Vui lòng chọn ảnh!</div>');
	}

	if(!isset($_POST['catagory-select']) || strlen($_POST['catagory-select'])<1)
	{
		//required variables are empty
		//die('<div class="alert alert-danger">Please select a category</div>');
		die('<div class="alert alert-danger">Vui lòng chọn chủ đề!</div>');
	}

	if(!isset($_POST['mName']) || strlen($_POST['mName'])<1)
	{
		//required variables are empty
		//die('<div class="alert alert-danger">Please add a title</div>');
		die('<div class="alert alert-danger">Vui lòng nhập tiêu đề!</div>');
	}
		
	if($_FILES['mFile']['error'])
	{
		//File upload error encountered
		die(upload_errors($_FILES['mFile']['error']));
	}

	//File property
	$file 					= $_FILES['mFile']['tmp_name']; 
    $sourceProperties		= getimagesize($file);     
	list($width, $height) 	= $sourceProperties;
	$imageType 				= $sourceProperties[2];
	
	//File title and File name
	$FileTitle		= mb_ucfirst($mysqli->escape_string(nl2br(htmlspecialchars($_POST['mName'])))); //File Title will be used as new File name
		
	$strFileTitle	= strlen($FileTitle);
	if ($strFileTitle > 20) {
		$FileName	= convertvn(substr($FileTitle,0,20));
	}else{
		$FileName	= convertvn($FileTitle);
	}

	
	switch ($imageType) {
		//allowed file types
		case IMAGETYPE_JPEG:
			$Type = "1";
			if($width > 600){
				$imageResourceId = imagecreatefromjpeg($file);
				$targetLayer = imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
				ob_start(); // start a new output buffer
  					imagejpeg($targetLayer);
					$imagedata = base64_encode(ob_get_clean());
				$content = fopen("data:image/jpg;base64,$imagedata", "r"); 	//data lake file content
			}else{
				$content = fopen($file, "r"); //data lake file content
			}
			$options->setContentType("image/jpg"); //data lake file type
			$ext = "jpg";
			break;

		case IMAGETYPE_PNG:
			$Type = "1";
			if($width > 600){
				$imageResourceId = imagecreatefrompng($file);
				$targetLayer = imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
				ob_start(); // start a new output buffer
					imagejpeg($targetLayer);
					$imagedata = base64_encode(ob_get_clean());
				$content = fopen("data:image/jpg;base64,$imagedata", "r"); 	//data lake file content
			}else{
				$content = fopen($file, "r"); //data lake file content
			}
			$options->setContentType("image/jpg"); //data lake file type
			$ext = "jpg";
			break;

		case IMAGETYPE_GIF:
			$Type = "2";
			if( filesize($file) > 3 * 1048576 ){
				die('<div class="alert alert-danger">Vui lòng chọn ảnh động kích thước nhỏ hơn 3M.</div>');
			}else{
				$content = fopen($file, "r");
			}
			$options->setContentType("image/gif"); //data lake file type
			$ext = "gif";
			break;
		
		default:
			die('<div class="alert alert-danger">Chọn sai định dạng. Vui lòng chọn lại ảnh!</div>'); //output error
			break;
	}
	
   //Rename and save uploded file to destination folder.
   if($Type == 1 || $Type == 2)
   {
	//Name catagory and post date
	$NewFileName	= $mysqli->escape_string(date("Ymd")."_".uniqid()."_".$FileName. ".". $ext);
	$Catagory		= $mysqli->escape_string($_POST['catagory-select']); 
	$Date			= (new DateTime('now', new DateTimeZone('Asia/Ho_Chi_Minh')))->format('c');
	
	// Insert info into database
	if($PostMedia = $mysqli->query("INSERT INTO media(title, image, type, catid, uid, date, active) VALUES ('$FileTitle','$NewFileName','$Type','$Catagory','$Uid','$Date','$Active')")) {

		try {
			//Upload file to data lake
			$blobClient->createBlockBlob($containerName, $NewFileName, $content, $options);
		}
		catch(ServiceException $e){
			$code = $e->getCode();
			$error_message = $e->getMessage();
		}
	
	}

	if (checkNumPosts() == -1) {
		$UpdateBonus = $mysqli->query("UPDATE users SET bonus=bonus-1 WHERE uid='$Uid' AND bonus>0");

		$Maxpost = checkbonus();
	} else {
		$Maxpost = checkNumPosts() + checkbonus();
	}

	// Free up memory
	unset($targetLayer);
	unset($imageResourceId);
	unset($imagedata);
	unset($content);

?>

<script>


function removeModel() {
$('#modelImages').modal('hide');
$('body').removeClass('modal-open');
$('.modal-backdrop').remove();
$("#output-image").empty();
$('#FileUploader').resetForm();
$('#mName').height(0);
$('#output_media').attr('src', '');
$('#output_media').hide();

var datapost = "<?php echo $Maxpost; ?>";
$('#postimage').attr('data-post', datapost);
$('#postvideo').attr('data-post', datapost);

var maxpost = "<?php echo number_format($Maxpost, 0, ',', '.'); ?>";
$('#maxpost').html(maxpost);
}

setTimeout(removeModel,1500);

</script>

<?php

	die('<div class="alert alert-info" style="text-align: center;">Cảm ơn bạn đã đăng bài!</div>');
		
   } else{
   		die('<div class="alert alert-danger">Đã xảy ra sự cố. Vui lòng thử lại!</div>');
   }
}

//Function to resize image
function imageResize($imageResourceId,$width,$height) {

    $targetWidth = 600;
    $targetHeight = round($targetWidth * $height / $width);

    $targetLayer=imagecreatetruecolor($targetWidth,$targetHeight);
    imagecopyresampled($targetLayer,$imageResourceId,0,0,0,0,$targetWidth,$targetHeight, $width,$height);

	return $targetLayer;   
}

// Check if number post limited
function checkNumPosts() {
	global $mysqli;
	global $Uid, $LimitPost;
	$BeginToday = strtotime((new DateTime('today', new DateTimeZone('Asia/Ho_Chi_Minh')))->format('c'));
	$MediaSql = "SELECT id FROM media WHERE uid='$Uid' AND UNIX_TIMESTAMP(date)>$BeginToday";
	$MediaResult = mysqli_query($mysqli, $MediaSql);
	$MediaCount = mysqli_num_rows($MediaResult);
	if ($MediaCount > $LimitPost) {
		$numPost = -1;
	} else {
		$numPost = $LimitPost - $MediaCount;
	}
	return $numPost;
}


function checkbonus() {
	global $mysqli;
	global $Uid;
	$MediaSql = "SELECT * FROM users WHERE uid='$Uid' limit 1";
	$MediaResult = mysqli_query($mysqli, $MediaSql);
	$MediaRow = mysqli_fetch_array($MediaResult);
	$Ubonus = $MediaRow['bonus'];
	return $Ubonus;
}


//function outputs upload error messages, http://www.php.net/manual/en/features.file-upload.errors.php#90522
function upload_errors($err_code) {
	switch ($err_code) { 
        case UPLOAD_ERR_INI_SIZE: 
            return '<div class="alert alert-danger" role="alert">Vui lòng chọn file có kích thước nhỏ hơn.</div>'; 
        case UPLOAD_ERR_FORM_SIZE: 
            return '<div class="alert alert-danger" role="alert">Vui lòng chọn file có kích thước nhỏ hơn.</div>'; 
        case UPLOAD_ERR_PARTIAL: 
            return '<div class="alert alert-danger" role="alert">File bị lỗi. Vui lòng kiểm tra lại.</div>'; 
        case UPLOAD_ERR_NO_FILE: 
            return '<div class="alert alert-danger" role="alert">Vui lòng chọn file.</div>'; 
        case UPLOAD_ERR_NO_TMP_DIR: 
            return '<div class="alert alert-danger" role="alert">Đã xảy ra lỗi.</div>'; 
        case UPLOAD_ERR_CANT_WRITE: 
            return '<div class="alert alert-danger" role="alert">Đã xảy ra lỗi.</div>'; 
        case UPLOAD_ERR_EXTENSION: 
            return '<div class="alert alert-danger" role="alert">File bị chặn.</div>'; 
        default: 
            return '<div class="alert alert-danger" role="alert">Đã xảy ra lỗi.</div>';  
    } 
} 

?>