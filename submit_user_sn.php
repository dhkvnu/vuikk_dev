<?php session_start();
include('../db.php');
include('convertvn.php');

if($_POST)
{	
	
	if(!isset($_POST['uNameSn']) || strlen($_POST['uNameSn'])<1)
	{
		//required variables are empty
		die('<div class="alert alert-danger" role="alert">Vui lòng nhập tên hiển thị!</div>');
	}

	if(strlen($_POST['uNameSn'])<3)
	{
		//required variables are empty
		die('<div class="alert alert-danger" role="alert">Tên hiển thị phải dài hơn 3 ký tự.</div>');
	}

	if(strlen($_POST['uNameSn'])>30)
	{
		//required variables are empty
		die('<div class="alert alert-danger" role="alert">Tên hiển thị phải ngắn hơn 30 ký tự.</div>');
	}
	
	if(!isset($_POST['uEmailSn']) || strlen($_POST['uEmailSn'])<1)
	{
		//required variables are empty
		die('<div class="alert alert-danger" role="alert">Vui lòng nhập địa chỉ email!</div>');
	}
	
	$email_address = $_POST['uEmailSn'];
	
	if (filter_var($email_address, FILTER_VALIDATE_EMAIL)) {
  	// The email address is valid
	} else {
  		die('<div class="alert alert-danger" role="alert">Email không hợp lệ.</div>');
	}
	
	///Get user info
	$uEmail = $mysqli->escape_string(strtolower($_POST['uEmailSn']));
	
	if($UserCheck = $mysqli->query("SELECT * FROM users WHERE email ='$uEmail'")){

   	$VdUser		= mysqli_fetch_array($UserCheck);
	
	$ACV	    = strtolower($VdUser['email']);

	$ACTIVE 	= $VdUser['u_active'];
	
   	$UserCheck->close();
   
	}else{
   
     //?>
	<script>
		errorpage();
	</script>
	<?php
	 printf("<div class='alert alert-danger alert-pull'>Đã xảy ra sự cố. Vui lòng thử lại! </div>");

	}
	
	if ($uEmail == $ACV && $ACTIVE == 1)
	{
		//required variables are empty
		die('<div class="alert alert-danger" role="alert">Email đã tồn tại. Vui lòng nhập email khác!</div>');
	}
	///
				
	//User info
	$UserName  			= mb_ucfirst($mysqli->escape_string($_POST['uNameSn'])); // Username
	$RegDate		    = (new DateTime('now', new DateTimeZone('Asia/Ho_Chi_Minh')))->format('c'); //date
	$SNID 				= $_SESSION['snid'];
	$PRV				= $_SESSION['provider'];
	$avatar             = $_SESSION['sn_pic'];

	//Registered but not yet active will update info!
	if ($uEmail == $ACV && $ACTIVE > 1){
		
		$mysqli->query("UPDATE users SET snid='$SNID', username='$UserName', password=NULL , provider='$PRV', avatar='$avatar', reg_date='$RegDate', u_active='1' WHERE email='$uEmail'");
		
	} else {  //Not yet registered will create new!
	
		$mysqli->query("INSERT INTO users(snid, username, email, provider, avatar, reg_date, u_active) VALUES ('$SNID', '$UserName', '$uEmail', '$PRV', '$avatar', '$RegDate', '1')");

	}

$_SESSION['useremail'] = $uEmail;

//Save to cookie
$cookie_expiration_time = time() + (86400 * 365);  // for 1 year
setcookie ("useremail", $uEmail, $cookie_expiration_time);
setcookie ("snid", $SNID, $cookie_expiration_time);

//unset old session
unset($_SESSION['snid']);
unset($_SESSION['sn_name']);
unset($_SESSION['sn_pic']);
unset($_SESSION['provider']);

?>
<script type="text/javascript">
function leave() {
  window.location = ".";
}
setTimeout("leave()", 0);
</script>
<?php

	//die('<div class="alert alert-success" role="alert">Đang đăng nhập</div>');		

   }else{
   		die('<div class="alert alert-danger" role="alert">Đã xảy ra sự cố. Vui lòng thử lại!</div>');
   } 

?>