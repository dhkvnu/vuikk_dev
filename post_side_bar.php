<div id="load_data_table">

<?php
//if ($Sidebar = $mysqli->query("SELECT id,title,image,type,feat FROM media WHERE feat=1 ORDER BY id DESC LIMIT 8")){
if ($Sidebar = $mysqli->query("SELECT * FROM media WHERE active=1 and catid='$PostCategoryId' ORDER BY RAND() LIMIT 5")){

	while($SidebarRow = mysqli_fetch_array($Sidebar)){
		
	$SidebarId = $SidebarRow['id'];
	$SidebarType = $SidebarRow['type']; 		
	$SidebarTitle= stripslashes($SidebarRow['title']);
	$strSidebar = strlen($SidebarTitle);
	if ($strSidebar > 72) {
	$SidebarMediaTitle = substr($SidebarTitle,0,72).'...';
	}else{
	$SidebarMediaTitle = $SidebarTitle;
	}
	
	$SidebarLink = convertvn($SidebarMediaTitle);
	$SidebarLink = nl2br($SidebarLink);
	$SidebarURL = "post-".$SidebarId."-".$SidebarLink.".html";
?>


    <div class="media-box">
	<a class="pull-left" href="<?php echo $SidebarURL; ?>" <?php if ($MediaOpen==1){?> target="_blank" <?php }?>>

<?php if($SidebarType=='1'){?>

	<img class="media-object" alt="" src="<?php echo $Settings['datalink']; ?>/uploads/<?php echo $SidebarRow['image'];?>" style="width: 220px; height: 120px; object-fit: cover;">

<?php } elseif($SidebarType=='2'){ ?>

<div style="width: 220px; height: 120px; overflow: hidden;">

	<div class="gif-img"><span class="glyphicon glyphicon-picture"></span> GIF</div>
	<img class="media-object" alt="" data-gifffer="<?php echo $Settings['datalink']; ?>/uploads/<?php echo $SidebarRow['image'];?>" style="width: 220px;">

</div>
	
<?php } elseif ($SidebarType=='3'){ ?>

	<?php if ($SidebarRow['video_type'] == "youtube") { ?>

		<div class="play-button"><span class="glyphicon glyphicon-facetime-video"></span>&nbsp; Video</div>
		<img class="media-object" alt="" src="https://img.youtube.com/vi/<?php echo $SidebarRow['video_id'];?>/hqdefault.jpg" style="width: 220px; height: 120px; object-fit: cover;">

	<?php } elseif ($SidebarRow['video_type'] =="facebook") { ?>
	<div style="width: 220px; height: 120px;">
		<div class="play-button"><span class="glyphicon glyphicon-facetime-video"></span>&nbsp; Video</div>
		<div class="video-frame-post"></div>
		<div class="fb-video" data-href="https://www.facebook.com/facebook/videos/<?php echo $SidebarRow['video_id']; ?>/" data-show-text="false" data-width="220" data-height="120" data-controls="false"></div>
	</div>
	<?php } ?>		

<?php }?>
</a>

	<div class="media-body-box">
		<a href="<?php echo $SidebarURL; ?>" <?php if ($MediaOpen==1){?> target="_blank" <?php }?>><h4 class="media-heading" style="word-wrap:break-word;"><?php echo $SidebarMediaTitle;?></h4></a>
		<h5><?php echo show_number($SidebarRow['views']);?> lượt xem</h5>
        <h5><?php echo show_number($SidebarRow['votes']); ?> thích</h5>
    </div>

  </div>

<?php     
	}
$Sidebar->close();
}else{
    ?>
	<script>
		errorpage();
	</script>
	<?php
}
?>

</div><!--featured-box-->

<div id="load_more" style="text-align: center; margin-top: 15px;">
	<button class="btn btn-default" name="btn_more" id="btn_more" type="button">+ Tải thêm</button>
</div>
