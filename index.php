<?php include("header.php");?>

<div class="container">

<div class="col-md-8" id="left">


<?php

//post button
include('post_button.php');

$timeindex  = time() - 86400; // 1 day ago

//Get top votes posts in 1 day
//if($LatestSql = $mysqli->query("SELECT * FROM media WHERE active=1 and UNIX_TIMESTAMP(date)>$timeindex ORDER BY votes DESC LIMIT 0, 8")){
if($LatestSql = $mysqli->query("SELECT * FROM media WHERE active=1 ORDER BY RAND() LIMIT 8")){
	if(mysqli_num_rows($LatestSql) < 8) {
		$LatestSql = $mysqli->query("SELECT * FROM media WHERE active=1 ORDER BY votes DESC LIMIT 0, 8");
	}
	
//Show posts
include("posts_body.php");

$LatestSql->close();
}else{
    ?>
	<script>
		errorpage();
	</script>
	<?php
}
?>

<nav id="page-nav"><a href="data_index.php?page=2"></a></nav>

<script src="js/jquery.infinitescroll.min.js"></script>
	<script src="js/manual-trigger.js"></script>
	
	<script>
	
	
	$('#left').infinitescroll({
		navSelector  : '#page-nav',    // selector for the paged navigation 
      	nextSelector : '#page-nav a',  // selector for the NEXT link (to page 2)
      	itemSelector : '.post-box',     //
		loading: {
          				finishedMsg: 'Đã tải xong',
						img: '<?php echo $Settings['datalink']; ?>/sysimg/ajax-loader.gif'
					}
	},
	function(){

		$.getScript('js/scroll_load.js');

	},
	function(newElements, data, url){
		//Gif Code
		$.getScript('js/ready_functions.js');	

	});
</script>

</div><!--/.col-md-8 -->

<div class="col-md-4">
<?php include("side_bar.php");?>
</div><!--/.col-md-4 -->

</div><!--/.container-->

<?php include("footer.php");?>

