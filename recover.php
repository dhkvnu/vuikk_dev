<?php include("header.php");?>

<div class="container">

<div class="col-md-8" id="left">


<script>
$(document).ready(function()
{
    $('#recoveredForm').on('submit', function(e)
    {
        e.preventDefault();
        $('.submitButton').attr('disabled', ''); // disable upload button
        //show uploading message
        $("#output").html('<div class="alert alert-info" role="alert">Đang xử lý ...</div>');
		
        $(this).ajaxSubmit({
        target: '#output',
        success:  afterSuccess //call function after success
        });
    });
});

</script>

<div class="post-box">
<header class="post-header"><div class="post-title-2"><h1>Cài đặt lại mật khẩu</h1></div><!--post-title--></header>

<form id="recoveredForm" action="send_recovery.php" method="post" style="margin-left: 10px; margin-right: 10px;">

<div id="output"></div>

<div class="form-group">
            <label for="inputRecovery">Email đăng nhập</label>
                <div class="input-group">
                   <span class="input-group-addon">@</span>
<input type="email" class="form-control" name="inputRecovery" id="inputRecovery" placeholder="Nhập email">
</div>
</div>
   
<button type="submit" class="btn btn-default btn-success pull-right submitButton" style="margin-bottom: 15px;">Xác nhận</button>

</form>

</div><!--post-box-->

</div><!--/.col-md-8 -->

<div class="col-md-4">
<?php include ("side_bar.php");?>
</div><!--/.col-md-4 -->

</div><!--/.container-->

<?php include("footer.php");?>

