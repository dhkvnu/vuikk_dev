<?php include("header.php");?>

<div class="container">

<div class="col-md-8" id="left">

<div class="post-box">
<header class="post-header"><div class="post-title-2"><h1>Liên hệ</h1></div><!--post-title--></header>


<script>
$(document).ready(function()
{
    $('#ContactForm').on('submit', function(e)
    {
        e.preventDefault();
        $('.submitButton').attr('disabled', ''); // disable upload button
        //show uploading message
        $("#output").html('<div class="alert alert-info" role="alert">Đang xử lý ...</div>');
		
        $(this).ajaxSubmit({
        target: '#output',
        success:  afterSuccess //call function after success
        });
    });
});

</script>

<div class="form-box">

<div id="output"></div>

<form id="ContactForm" action="send_mail.php" method="post">

    <div class="form-group">
        <label for="inputYourname">Họ tên</label>
        <div class="input-group">
            <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
            <input type="text" class="form-control" name="inputYourname" id="inputYourname" placeholder="Nhập họ tên">
        </div>
    </div>

    <div class="form-group">
        <label for="inputEmail">Email</label>
        <div class="input-group">
            <span class="input-group-addon">@</span>
            <input type="email" class="form-control" name="inputEmail" id="inputEmail" placeholder="Nhập email">
        </div>
    </div>

    <div class="form-group">
        <label for="inputSubject">Tiêu đề</label>
        <div class="input-group">
            <span class="input-group-addon"><span class="glyphicon glyphicon-info-sign"></span></span>
            <input type="text" class="form-control" name="inputSubject" id="inputSubject" placeholder="Nhập tiêu đề">
        </div>
    </div>

    <div class="form-group">
        <label for="inputMessage">Nội dung</label>
        <textarea class="form-control" id="inputMessage" name="inputMessage" rows="3" placeholder="Nhập nội dung"></textarea>
    </div>        
       
<button type="submit" class="btn btn-default btn-primary pull-right submitButton">Gửi</button>

</form>
</div>

</div><!--post-box-->

</div><!--/.col-md-8 -->

<div class="col-md-4">
<?php include ("side_bar.php");?>
</div><!--/.col-md-4 -->

</div><!--/.container-->

<?php include("footer.php");?>

