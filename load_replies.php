<?php session_start();
include('../db.php');
include('convertvn.php');

if($squ = $mysqli->query("SELECT * FROM settings WHERE id='1'")){

    $Settings = mysqli_fetch_array($squ);	
	$squ->close();
	
}else{

	?><script>errorpage();</script><?php
	
}


if(isset($_SESSION['useremail'])){

$uEmail = $_SESSION['useremail'];

if($UserSql = $mysqli->query("SELECT * FROM users WHERE email='$uEmail'")){

  $UserRow = mysqli_fetch_array($UserSql);

	$Uid = $UserRow['uid'];
	
  $UserSql->close();
	
}else{
     
	?><script>errorpage();</script><?php
	 
}
} else {
	$Uid = 0;
}


if (isset($_POST['pid']))
{

$pid=$_POST['pid'];
$cid=$_POST['cid'];

$pid = $mysqli->real_escape_string($pid);
$cid = $mysqli->real_escape_string($cid);


$Rnumber = 0;
$Rlimit = 5;

if($Replies = $mysqli->query("SELECT * FROM comments LEFT JOIN users ON comments.uid=users.uid WHERE comments.pid='$pid' AND comments.parent='$cid' ORDER BY comments.id ASC")){

$RepliesNumbers = mysqli_num_rows($Replies);

	while ($Reply = mysqli_fetch_array($Replies)){

    $ReplyId = $Reply['id'];
    $ParentId = $Reply['parent'];

	$ReplyUserId = $Reply['uid'];
	$ReplyAvatar = $Reply['avatar'];
	$ReplyDate = $Reply['date'];
	$ReplyToUserId = $Reply['to_uid']; 
	$ReplyFull = $Reply['comment'];
	$ReplyVotes = $Reply['votes'];
	$ReplyEdited = $Reply['edited'];

	$ReplyUserName = $Reply['username'];
	$ReplyUserLink = convertvn($ReplyUserName);
	$ReplyUserUrl = "profile-".$ReplyUserId."-".$ReplyUserLink.".html";

	if($ToUser = $mysqli->query("SELECT username FROM users WHERE uid='$ReplyToUserId'")){
		$ToUserList = mysqli_fetch_array($ToUser);
		$ReplyToUserName = $ToUserList['username'];
	$ToUser->close();
	}

//Replies content
$output = '';

$output .= '

<div class="replies-box" id="comment-'.$ParentId.'-'.$Rnumber.'" style="display: none;">

	<a href="'.$ReplyUserUrl.'">';
 		if (empty($ReplyAvatar)){ 
			$output .= '<img style="width: 20px; height: 20px; border-radius: 50%; object-fit: cover;" src="'.$Settings['datalink'].'/sysimg/default-avatar.png" alt="avatar" class="media-object pull-left" />';
		}elseif (substr($ReplyAvatar,0,4) == "http"){
			$output .= '<img style="width: 20px; height: 20px; border-radius: 50%; object-fit: cover;" src="'.$ReplyAvatar.'" alt="avatar" class="media-object pull-left" />';
		}else {
			$output .= '<img style="width: 20px; height: 20px; border-radius: 50%; object-fit: cover;" src="'.$Settings['datalink'].'/avatars/'.$ReplyAvatar.'" alt="avatar" class="media-object pull-left" />';
 		} $output .= '
	</a>

<div class="media-replies-body" id="comment-body-'.$ReplyId.'">

    <h4 class="media-heading" style="font-size: 12px;"><a href="'.$ReplyUserUrl.'">'.$ReplyUserName.'</a><span style="color: #999; font-weight: normal;">&nbsp;'.get_time_ago(strtotime($ReplyDate)).'<span id="cmtEdited-'.$ReplyId.'">'; if ( $ReplyEdited == 1 ) { $output .= ' &bull; Đã sửa'; } $output .= '</span></span></h4>
    <h5 style="word-wrap:break-word;"><b style="color: #337AB7;">'.$ReplyToUserName.'</b>&nbsp;<span id="cmtContent-'.$ReplyId.'">'.$ReplyFull.'</span></h5>

    <div class="comment-vote-box">';

	if(isset($_SESSION['useremail'])){ $output .= '
	<i '; if (cmtLiked($ReplyId)): $output .= 'class="btn fas fa-thumbs-up"';
	else: $output .= 'class="btn far fa-thumbs-up"'; endif;
	$output .= ' data-id="'.$ReplyId.'" onclick="voteup_comment(this)" style="font-size: 14px;"></i>
	
	<span class="display-vote-comment" data-points="'.$ReplyVotes.'">'.show_number($ReplyVotes).'</span>
	
	<i '; if (cmtDisliked($ReplyId)): $output .= 'class="btn fas fa-thumbs-down"';
	else: $output .= 'class="btn far fa-thumbs-down"'; endif;
	$output .= ' data-id="'.$ReplyId.'" onclick="votedown_comment(this)" style="font-size: 14px;"></i>';
	} else { 
	$output .= '
	<i class="btn far fa-thumbs-up" data-toggle="modal" data-target="#myModal" title="Vui lòng đăng nhập!" style="font-size: 14px;"></i>
	
	<span class="display-vote-comment">'.show_number($ReplyVotes).'</span>
	
	<i class="btn far fa-thumbs-down" data-toggle="modal" data-target="#myModal" title="Vui lòng đăng nhập!" style="font-size: 14px;"></i>';
	}
	
	if(isset($_SESSION['useremail'])){
		if($ReplyUserId == $Uid) { $output .= '
			<a href="javascript:void(0)" id="editcmt_content-'.$ReplyId.'" data-id="'.$ReplyId.'" data-content="'.str_replace("<br />", "\n", $ReplyFull).'" onclick="edit_comment(this)" style="color: #065FD4;">Sửa</a>';
		} else { $output .= '
			<a href="javascript:void(0)" data-parentID="'.$ParentId.'" data-toUserId="'.$ReplyUserId.'" data-toUserName="'.$ReplyUserName.'" onclick="reply(this)" style="color: #065FD4;">Trả lời</a>';
		} 
	} $output .= '
	
			</div><!--comment-vote-box-->

		</div><!--media-body-->

    </div><!--comment-box-->';

echo $output;

//Show 5 first replies
if( $Rnumber < $Rlimit ){
	?>
	<script>
		var i = "<?php echo $ParentId.'-'.$Rnumber; ?>";
		$('#comment-' + i).css( 'display', 'block' );
	</script>
	
	<?php
}

$Rnumber++;
	}

$Replies->close();
} else{
   
    printf("<div class='alert alert-danger alert-pull'>Đã xảy ra sự cố. Vui lòng thử lại!</div>");

}
if($Rlimit < $RepliesNumbers){
    ?>
    <div class="more_reply">
        <a href="javascript:void(0)" name="morerep-btn" id="morerep-btn" data-parent=<?php echo $ParentId; ?> data-id=<?php echo $Rlimit; ?> data-rows=<?php echo $RepliesNumbers; ?> type="button" style="font-size: 14px; color: #065FD4;">Xem thêm trả lời</a>
    </div>
    <br>
<?php }
}

// Check if user already likes post or not
function cmtLiked($cmt_id)
{
  global $mysqli;
  global $Uid;
  $sql = "SELECT * FROM votecmt WHERE uid=$Uid AND cmt_id=$cmt_id AND type='1'";
  $result = mysqli_query($mysqli, $sql);
  if (mysqli_num_rows($result) > 0) {
  	return true;
  }else{
  	return false;
  }
}

// Check if user already dislikes post or not
function cmtDisliked($cmt_id)
{
  global $mysqli;
  global $Uid;
  $sql = "SELECT * FROM votecmt WHERE uid=$Uid AND cmt_id=$cmt_id AND type='2'";
  $result = mysqli_query($mysqli, $sql);
  if (mysqli_num_rows($result) > 0) {
  	return true;
  }else{
  	return false;
  }
}

?>