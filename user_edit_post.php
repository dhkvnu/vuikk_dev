<?php
include('../db.php'); 

if(isset($_POST['id'])) {

$id = $mysqli->escape_string($_POST['id']); 

?>

<script>
$(document).ready(function()
{
    $('#FormEditPost').on('submit', function(e)
    {
        e.preventDefault();
        $('.submitButton').attr('disabled', ''); // disable upload button
        //show uploading message
        $("#output-post").html('<div class="alert alert-info" role="alert">Đang xử lý ...</div>');
		
        $(this).ajaxSubmit({
        target: '#output-post',
        success:  afterSuccess
        });
    });
});

</script>


<?php


if($Post = $mysqli->query("SELECT * FROM media WHERE id='$id'")){

    $PostRow = mysqli_fetch_array($Post);
	
    $SelectedCat = $PostRow['catid'];
    
    $SelectedTitle = str_replace("<br />", "", $PostRow['title']);
	
    $Post->close();
	
}else{
    
	?>
	<script>
		errorpage();
	</script>
	<?php
}

$output = '';

$output .= ' 

<form id="FormEditPost" action="user_update_post.php?id='.$id.' enctype="multipart/form-data" method="post">

<div class="form-group">
<label for="category">Sửa chủ đề</label>
<select class="form-control" name="category" id="category">
'; //output 1

if($CatSelected = $mysqli->query("SELECT * FROM categories WHERE id='$SelectedCat' LIMIT 1")){

($CatSelectedRow = mysqli_fetch_array($CatSelected));

$SelectedCat = $CatSelectedRow['id'];

$output .= '   
	<option value='.$CatSelectedRow['id'].'>'.$CatSelectedRow['cname'].'</option>
'; //output 2

$CatSelected->close();

}else{
    ?><script>errorpage();</script><?php
}

if($CatSql = $mysqli->query("SELECT * FROM categories WHERE id!='$SelectedCat' ORDER BY cname ASC")){

    while ($CatRow = mysqli_fetch_array($CatSql)){
		
$output .= '
    <option value='.$CatRow['id'].'>'.$CatRow['cname'].'</option>
'; //output 3  
   
}
$CatSql->close();
}else{
    ?><script>errorpage();</script><?php
}

$output .= '
</select>
</div>

<div class="form-group">
<label for="inputTitle">Sửa tiêu đề</label>
<textarea class="form-control" name="inputTitle" id="inputTitle" data-id="'.$SelectedTitle.'" rows=1 placeholder="Nhập tiêu đề mới"></textarea>
</div>

<div id="output-post" style="text-align:center;"></div>
<div class="col text-center">
    <input type="button" class="btn btn-default" data-dismiss="modal" value="Hủy" style="margin-right: 5px;" />
    <input type="submit" class="btn btn-primary submitButton" value="Xác nhận" />
</div>

</form>
'; //output 4


echo $output; 

}
?>