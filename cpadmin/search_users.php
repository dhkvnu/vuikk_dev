<?php include("header.php");?>

<section class="col-md-2">

<?php include("left_menu.php");
$term = $mysqli->escape_string($_GET['term']);
?>
                    
</section><!--col-md-2-->

<section class="col-md-10">

<ol class="breadcrumb">
  <li>Quản trị</li>
  <li class="active">Search Users</li>
</ol>

<div class="page-header">
  <h3>Search Users <small>Search your website users</small></h3>
</div>

<script type="text/javascript">
$(document).ready(function(){
//Delete	
$('button.btnDelete').on('click', function (e) {
    e.preventDefault();
    var id = $(this).closest('tr').data('id');
    $('#myModal').data('id', id).modal('show');
});

$('#btnDelteYes').click(function () {
    var id = $('#myModal').data('id');
	var dataString = 'id='+ id ;
    $('[data-id=' + id + ']').remove();
    $('#myModal').modal('hide');
	//ajax
	$.ajax({
type: "POST",
url: "delete_user.php",
data: dataString,
cache: false,
success: function(html)
{

$("#output").html(html);
}
});
//ajax ends
});
});
</script>

<section class="col-md-8">

<div id="custom-search-input">
       <form class="input-group col-md-12" role="search" name="srch-term" id="srch-term" method="get" action="search_users.php">
           <input type="text" class="search-query form-control" id="term" name="term" placeholder="Search Users by Username or Email" value="<?php echo $term;?>"/>
                     <span class="input-group-btn">
                           <button class="btn btn-info" type="submit">
                          <span class=" glyphicon glyphicon-search"></span>
                 </button>
            </span>
     </form>
                        
</div>

<div id="output"></div>
     
<?php
error_reporting(E_ALL ^ E_NOTICE);
// How many adjacent pages should be shown on each side?
	$adjacents = 5;
	
	$query = $mysqli->query("SELECT COUNT(*) as num FROM users WHERE (username like '%$term%' OR email like '%$term%') AND u_active=1 ORDER BY uid DESC");
	
	//$query = $mysqli->query("SELECT COUNT(*) as num FROM photos WHERE  photos.active=1 ORDER BY photos.id DESC");
	
	$total_pages = mysqli_fetch_array($query);
	$total_pages = $total_pages['num'];
	
	$targetpage = "search_users.php";
	$limit = 15; 								//how many items to show per page
	$page = $mysqli->escape_string($_GET['page']);
	 
	if($page) 
		$start = ($page - 1) * $limit; 			//first item to display on this page
	else
		$start = 0;								//if no page var is given, set start to 0
 	/* Get data. */
	$result = $mysqli->query("SELECT * FROM users WHERE (username like '%$term%' OR email like '%$term%') AND u_active=1 ORDER BY uid DESC LIMIT $start, $limit");
	 
	//$result = $mysqli->query($sql);
	
	/* Setup page vars for display. */
	if ($page == 0) $page = 1;					//if no page var is given, default to 1.
	$prev = $page - 1;							//previous page is page - 1
	$next = $page + 1;							//next page is page + 1
	$lastpage = ceil($total_pages/$limit);		//lastpage is = total pages / items per page, rounded up.
	$lpm1 = $lastpage - 1;						//last page minus 1
	
	$pagination = "";
	if($lastpage > 1)
	{	
		$pagination .= "<ul class=\"pagination pagination-lg\">";
		//previous button
		if ($page > 1) 
			$pagination.= "<li><a href=\"$targetpage?page=$prev\">&laquo;</a></li>";
		else
			$pagination.= "<li class=\"disabled\"><a href=\"#\">&laquo;</a></li>";	
		
		//pages	
		if ($lastpage < 7 + ($adjacents * 2))	//not enough pages to bother breaking it up
		{	
			for ($counter = 1; $counter <= $lastpage; $counter++)
			{
				if ($counter == $page)
					$pagination.= "<li class=\"active\"><a href=\"#\">$counter</a></li>";
				else
					$pagination.= "<li><a href=\"$targetpage?page=$counter\">$counter</a></li>";					
			}
		}
		elseif($lastpage > 5 + ($adjacents * 2))	//enough pages to hide some
		{
			//close to beginning; only hide later pages
			if($page < 1 + ($adjacents * 2))		
			{
				for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
				{
					if ($counter == $page)
						$pagination.= "<li class=\"active\"><a href=\"#\">$counter</a></li>";
					else
						$pagination.= "<li><a href=\"$targetpage?page=$counter\">$counter</a></li>";					
				}
				$pagination.= "...";
				$pagination.= "<li><a href=\"$targetpage?page=$lpm1\">$lpm1</a></li>";
				$pagination.= "<li><a href=\"$targetpage?page=$lastpage\">$lastpage</a></li>";		
			}
			//in middle; hide some front and some back
			elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
			{
				$pagination.= "<li><a href=\"$targetpage?page=1\">1</a></li>";
				$pagination.= "<li><a href=\"$targetpage?page=2\">2</a></li>";
				$pagination.= "...";
				for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
				{
					if ($counter == $page)
						$pagination.= "<li class=\"active\"><a href=\"#\">$counter</a></li>";
					else
						$pagination.= "<li><a href=\"$targetpage?page=$counter\">$counter</a></li>";					
				}
				$pagination.= "...";
				$pagination.= "<li><a href=\"$targetpage?page=$lpm1\">$lpm1</a></li>";
				$pagination.= "<li><a href=\"$targetpage?page=$lastpage\">$lastpage</a></li>";		
			}
			//close to end; only hide early pages
			else
			{
				$pagination.= "<li><a href=\"$targetpage?page=1\">1</a></li>";
				$pagination.= "<li><a href=\"$targetpage?page=2\">2</a></li>";
				$pagination.= "...";
				for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
				{
					if ($counter == $page)
						$pagination.= "<li class=\"active\"><a href=\"#\">$counter</a></li>";
					else
						$pagination.= "<li><a href=\"$targetpage?page=$counter\">$counter</a></li>";					
				}
			}
		}
		
		//next button
		if ($page < $counter - 1) 
			$pagination.= "<li><a href=\"$targetpage?page=$next\">&raquo;</a></li>";
		else
			$pagination.= "<li class=\"disabled\"><a href=\"#\">&raquo;</a></li>";
		$pagination.= "</ul>\n";		
	}
	
	$q= $mysqli->query("SELECT * FROM users WHERE (username like '%$term%' OR email like '%$term%') AND u_active=1 ORDER BY uid DESC limit $start,$limit");


	$numr = mysqli_num_rows($q);
	if ($numr==0)
	{
	?>
    
	<div class="no-results">
	<h3>Tìm kiếm "<span class="tt-text"><?php echo $term;?></span>" không có kết quả nào.</h3>
	<ul class="search-again">
	<li>Hãy chắc chắn nhập đúng từ khóa.</li>
	<li>Thử lại với từ khóa khác.</li>
	<li>Thêm từ khóa vào tìm kiếm.</li>
    </ul>
	</div>
	<?php
	}
	if ($numr>0){
	?>
       <table class="table table-bordered">

        <thead>

            <tr>
            
            	<th>Profile Photo</th>
                
                <th>Username</th>

                <th>Email</th>
                
                <th>Registered Date</th>

                <th>Quản lý</th>

            </tr>

        </thead>

        <tbody>
    <?php
	}
	
	while($Row=mysqli_fetch_assoc($q)){
	
	$Avatar   = $Row['avatar'];
	$User	  = stripslashes($Row['username']);

	$UserLink = convertvn($User);
	
?>        

            <tr class="btnDelete" data-id="<?php echo $Row['uid'];?>">
				<td><a href="../profile-<?php echo $Row['uid'];?>-<?php echo $UserLink;?>.html" target="_blank">      
                
        <?php if (empty($Avatar)){ ?>   
    		<img src="<?php echo $Settings['datalink']; ?>/sysimg/default-avatar.png" width="50" height="50" alt="avatar" class="img-responsive"/>
    
		<?php }elseif (substr($Uavatar,0,4) == "http"){?>
			<img id="avatar-img" src="<?php echo $Uavatar; ?>" alt="avatar" class="avatar-profile"/>
		
		<?php }else{?>
			<img src="<?php echo $Settings['datalink']; ?>/avatars/<?php echo $Avatar;?>" alt="avatar" style="width: 50px; height: 50px; object-fit: cover;" class="img-responsive"/>

		<?php }?>
                
                </a></td>
                
                <td><a href="../profile-<?php echo $Row['uid'];?>-<?php echo $UserLink;?>.html" target="_blank"><?php echo $Row['username'];?></a></td>

                <td><?php echo $Row['email'];?></td>
				
                <td><?php echo $Row['reg_date'];?></td>
                
                <td>
                <!--Testing Modal-->
                
               <button class="btn btn-danger btnDelete">Delete</button>
                                 
                <!--Testing Modal-->
                </td>

            </tr>
<?php } ?>
         
        </tbody>

    </table>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title">Xác nhận</h4>

            </div>
            <div class="modal-body">
				<p>Are you sure you want to DELETE this User?</p>
                <p class="text-danger"><small>Please not that all the posts and comments posted by this user will also be deleted.</small></p>
                <p class="text-warning"><small>This action cannot be undone.</small></p>		
            </div>
            <!--/modal-body-collapse -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" id="btnDelteYes">Yes</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
            </div>
            <!--/modal-footer-collapse -->
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal --> 

<?php echo $pagination;?>

</section>

</section><!--col-md-10-->

<?php include("footer.php");?>