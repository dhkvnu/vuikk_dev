<?php session_start();
ob_start();

if(isset($_SESSION['adminuser'])){
	header("location:index.php");
}


include("../../db.php");

if($SettingsSql = $mysqli->query("SELECT * FROM settings WHERE id='1'")){

  $Settings = mysqli_fetch_array($SettingsSql);

  $SettingsSql->close();

} else {
  ?><script>errorpage();</script><?php
}


?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title><?php echo $Settings['name']; ?> - Admin</title>
<meta name="author" content="FlippyScripts">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=0">

<link href="<?php echo $Settings['datalink']; ?>/sysimg/favicon.ico" rel="shortcut icon" type="image/x-icon"/>

<link href="css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="css/style.css" rel="stylesheet" type="text/css">
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<script src="js/jquery.min.js"></script>	
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.pjax.js" type="text/javascript"></script>
<script src="js/jquery.ui.shake.js"></script>

<script>
$(function(){
  $(document).pjax('a', '.main-header');
});
</script>

</head>

<body>
<!--
Vui KK Copyright!
-->
<div id="wrap">

<div class="container-fluid">

<header class="main-header" style="background: #dddddd;">

<a class="logo" href="index.php"><img class="img-responsive" src="<?php echo $Settings['datalink']; ?>/sysimg/logo.png" alt="logo" style="height: 35px;"></a>

</header>