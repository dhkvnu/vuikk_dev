<?php include("header.php");?>

<section class="col-md-2">

<?php include("left_menu.php");?>
                    
</section><!--col-md-2-->

<section class="col-md-10">

<ol class="breadcrumb">
  <li>Quản trị</li>
  <li class="active">Thông tin</li>
</ol>

<div class="page-header">
  <h3>Thông tin <small>Hiển thị thông tin</small></h3>
</div>

<section class="col-md-8">

<section class="col-md-6 box-space-right">

<div class="panel panel-default">

<div class="panel-heading"><h4>Thông tin trang</h4></div>

    <div class="panel-body">

<ul>

	<li><span>Tổng lượt truy cập: <?php echo number_format($Settings['site_hits'], 0, ',', '.');?></span></li>
 
</ul>

</div>

</div><!--panel panel-default-->  

</section><!--col-md-6-->

<section class="col-md-6">

<div class="panel panel-default">

<div class="panel-heading"><h4>Thông tin người dùng</h4></div>

    <div class="panel-body">

<ul>

<?php 
if($TotalUsers = $mysqli->query("SELECT uid FROM users WHERE u_active=1")){

    $TotalUNum = $TotalUsers->num_rows;
  
?>      
    <li><span>Đã kích hoạt: <?php echo number_format($TotalUNum, 0, ',', '.');?></span></li>
<?php

    $TotalUsers->close();
	
}

if($TotalPendingUsers = $mysqli->query("SELECT uid FROM users WHERE u_active!=1")){

	$PendingUNum = $TotalPendingUsers->num_rows;

?>

	<li><span>Chưa kích hoạt: <?php echo number_format($PendingUNum, 0, ',', '.')." (".round(($PendingUNum / $TotalUNum) * 100)."%)"; ?></span></li> 

<?php

    $TotalPendingUsers->close();
	
}
?>
</ul>

</div>

</div><!--panel panel-default--> 

</section><!--col-md-6-->
</section><!--col-md-8-->



<!-- Post info -->

<section class="col-md-8">

<section class="col-md-6 box-space-right">

<div class="panel panel-default">

<div class="panel-heading"><h4>Thông tin ảnh</h4></div>

    <div class="panel-body">

<ul>

<?php
if($PictureNumber = $mysqli->query("SELECT id FROM media WHERE type=1 or type=2")){

    $TotalNumbePic = $PictureNumber->num_rows;
  
?> 
     <li class="fa fa-picture-o"><span>Tổng số ảnh: <?php echo number_format($TotalNumbePic, 0, ',', '.'); ?></span></li>

<?php

    $PictureNumber->close();
	
}

if($ApprovedPicturs= $mysqli->query("SELECT id FROM media WHERE active=1 and (type=1 or type=2)")){

    $ApprovedPicNumber = $ApprovedPicturs->num_rows;
?>     

	<li class="fa fa-picture-o"><span>Đã duyệt: <?php echo number_format($ApprovedPicNumber, 0, ',', '.')." (".round(($ApprovedPicNumber / $TotalNumbePic) * 100)."%)"; ?></span></li>

<?php

    $ApprovedPicturs->close();
	
}

if($PendingPictures = $mysqli->query("SELECT id FROM media WHERE active=0 and (type=1 or type=2)")){

    $PendingPicNumber= $PendingPictures->num_rows;
?>      
    <li class="fa fa-picture-o"><span>Chưa duyệt: <?php echo number_format($PendingPicNumber, 0, ',', '.')." (".round(($PendingPicNumber / $TotalNumbePic) * 100)."%)"; ?></span></li>
<?php

    $PendingPictures->close();
	
}

if($BreakPictures = $mysqli->query("SELECT id FROM media WHERE active<0 and (type=1 or type=2)")){

    $BreakPicNumber= $BreakPictures->num_rows;
?>      
    <li class="fa fa-picture-o"><span>Vi phạm: <?php echo number_format($BreakPicNumber, 0, ',', '.')." (".round(($BreakPicNumber / $TotalNumbePic) * 100)."%)"; ?></span></li>
<?php

    $BreakPictures->close();
	
}

?> 
</ul>

</div>

</div><!--panel panel-default-->  

</section><!--col-md-6-->

<section class="col-md-6">

<div class="panel panel-default">

<div class="panel-heading"><h4>Thông tin video</h4></div>

    <div class="panel-body">

<ul>

<?php 
if($TotalVideos = $mysqli->query("SELECT id FROM media WHERE type=3")){

    $TotalVidNum = $TotalVideos->num_rows;
  
?>      
    <li class="fa fa-video-camera"><span>Tổng Video: <?php echo number_format($TotalVidNum, 0, ',', '.'); ?></span></li>
<?php

    $TotalVideos->close();
	
}

if($TotalApprovedVideos = $mysqli->query("SELECT id FROM media WHERE type=3 and active=1")){

    $ApprovedVidNum = $TotalApprovedVideos->num_rows;
?>

<li class="fa fa-video-camera"><span>Đã duyệt: <?php echo number_format($TotalVidNum, 0, ',', '.')." (".round(($ApprovedVidNum / $TotalVidNum) * 100)."%)"; ?></span></li> 

<?php

    $TotalApprovedVideos->close();
	
}

if($TotalPendingVideos = $mysqli->query("SELECT id FROM media WHERE type=3 and active=0")){

    $PendingVidNum = $TotalPendingVideos->num_rows;
  
?>    
    <li class="fa fa-video-camera"><span>Chưa duyệt: <?php echo number_format($PendingVidNum, 0, ',', '.')." (".round(($PendingVidNum / $TotalVidNum) * 100)."%)"; ?></span></li>

<?php

    $TotalPendingVideos->close();
	
}

if($BreakVideos = $mysqli->query("SELECT id FROM media WHERE type=3 and active<0")){

    $BreakVidNum = $BreakVideos->num_rows;
  
?>    
    <li class="fa fa-video-camera"><span>Vi phạm: <?php echo number_format($BreakVidNum, 0, ',', '.')." (".round(($BreakVidNum / $TotalVidNum) * 100)."%)"; ?></span></li>

<?php

    $BreakVideos->close();
	
}

?>
</ul>

</div>

</div><!--panel panel-default--> 

</section><!--col-md-6-->
</section><!--col-md-8-->



<section class="col-md-8 box-space-top">

<div class="panel panel-default">

<div class="panel-heading"><h4>10 bài đã duyệt mới nhất</h4></div>

    <div class="panel-body">

<?php

$DisplayApproved= $mysqli->query("SELECT * FROM media WHERE active=1 ORDER BY id DESC LIMIT 10");

	$NumberOfApp = $DisplayApproved->num_rows;
	
	if ($NumberOfApp==0)
	{
	echo '<div class="alert alert-danger">There are no approved posts to display at this moment.</div>';
	}
	if ($NumberOfApp>0)
	{
	?>
       <table class="table table-bordered">

        <thead>

            <tr>
				<th>Thumb</th>
                
                <th>Tiêu đề</th>

                <th>Thể loại</th>

                <th>Thời gian</th>
                
            </tr>

        </thead>

        <tbody>
    <?php
	}
	
	while($AppRow = mysqli_fetch_assoc($DisplayApproved)){
	
	$AppLongTitle = stripslashes($AppRow['title']);
	$SortAppTitle = short_title($AppLongTitle);
	
	$Type = $AppRow['type'];
	
	$AppPostLink = convertVn($SortAppTitle);

?>        

            <tr>
				<td><a href="../post-<?php echo $AppRow['id'];?>-<?php echo $AppPostLink;?>.html" target="_blank">
                <?php if($Type==1 || $Type==2){?> 
                	<img src="<?php echo $Settings['datalink']; ?>/uploads/<?php echo $AppRow['image'];?>" alt="image" style="width: 50px; height: 50px; object-fit: cover;" class="img-responsive">
                <?php }elseif($Type==3){
                    
                    if ($AppRow['video_type'] == "youtube") { ?>
                        <img class="featured-imgBar" alt="video" src="https://img.youtube.com/vi/<?php echo $AppRow['video_id'];?>/hqdefault.jpg" style="width: 50px; height: 50px; object-fit: cover;" class="img-responsive">
                    <?php } else if ($AppRow['video_type'] =="facebook") { ?>
                        <div style="width: 50px; height: 50px; overflow: hidden;">
                            <div class="fb-video" data-href="https://www.facebook.com/facebook/videos/<?php echo $AppRow['video_id']; ?>/" data-show-text="false" data-width="50" data-height="50" data-controls="false"></div>
                        </div>
                    <?php }
                }?>
                </a></td>
                
                <td><a href="../post-<?php echo $AppRow['id'];?>-<?php echo $AppPostLink;?>.html" target="_blank"><?php echo $SortAppTitle;?></a></td>

                <td>
                <?php if($Type==1){
					echo "Ảnh";
				}elseif($Type==2){
					echo "Gif";	
				}elseif($Type==3){
					echo "Video";
				}
				?>
                </td>

				<td><?php echo get_time_ago(strtotime($AppRow['date']));?></td>

            </tr>
<?php } ?>
    
         
        </tbody>

    </table>
    

</div>

</div><!--panel panel-default--> 

</section><!--col-md-8-->


<section class="col-md-8 box-space-top">

<div class="panel panel-default">

<div class="panel-heading"><h4>10 bài chưa duyệt mới nhất</h4></div>

    <div class="panel-body">

<?php

$DisplayPending= $mysqli->query("SELECT * FROM media WHERE active<1 ORDER BY id DESC LIMIT 8");

	$NumberOfPen = $DisplayPending->num_rows;
	
	if ($NumberOfPen==0)
	{
	echo '<div class="alert alert-danger">Chưa có bài đăng</div>';
	}
	if ($NumberOfPen>0)
	{
	?>
       <table class="table table-bordered">

        <thead>

            <tr>
				<th>Thumb</th>
                
                <th>Tiêu đề</th>

                <th>Thể loại</th>

                <th>Thời gian</th>

                <th>Trạng thái</th>
                
            </tr>

        </thead>

        <tbody>
    <?php
	}
	
	while($PenRow = mysqli_fetch_assoc($DisplayPending)){
	
	$PenLongTitle = stripslashes($PenRow['title']);
	$SortPenTitle = short_title($PenLongTitle);
	
    $PenType = $PenRow['type'];
    $Active = $PenRow['active'];
	
	$PenPostLink = convertVn($SortPenTitle);

?>        

            <tr>
				<td><a href="../post-<?php echo $PenRow['id'];?>-<?php echo $PenPostLink;?>.html" target="_blank">
                <?php if($PenType==1 || $PenType==2){?> 
                <img src="<?php echo $Settings['datalink']; ?>/uploads/<?php echo $PenRow['image'];?>" alt="image" style="width: 50px; height: 50px; object-fit: cover;" class="img-responsive">
                <?php }elseif($PenType==3){
                    if ($PenRow['video_type'] == "youtube") { ?>
                        <img class="featured-imgBar" alt="video" src="https://img.youtube.com/vi/<?php echo $PenRow['video_id'];?>/hqdefault.jpg" style="width: 50px; height: 50px; object-fit: cover;" class="img-responsive">
                    <?php } else if ($PenRow['video_type'] =="facebook") { ?>
                        <div style="width: 50px; height: 50px; overflow: hidden;">
                        <div class="fb-video" data-href="https://www.facebook.com/facebook/videos/<?php echo $PenRow['video_id']; ?>/" data-show-text="false" data-width="50" data-height="50" data-controls="false"></div>
                        </div>
                    <?php }
                }?>
                </a></td>
                
                <td><a href="../post-<?php echo $PenRow['id'];?>-<?php echo $PenPostLink;?>.html" target="_blank"><?php echo $SortPenTitle;?></a></td>

                <td>
                <?php if($PenType==1){
					echo "Ảnh";
				}elseif($PenType==2){
					echo "Gif";	
				}elseif($PenType==3){
					echo "Video";
				}
                ?>
                </td>

                <td><?php echo get_time_ago(strtotime($PenRow['date']));?></td>
                
                <td>
                <?php if($Active==0){
					echo "Chưa duyệt";
				}elseif($Active==-1){
					echo "Chưa hay";	
				}elseif($Active==-2){
					echo "Vi phạm";
				}elseif($Active==-3){
					echo "Quản trị";
				}
                ?>
                </td>

            </tr>
<?php } ?>
    
         
        </tbody>

    </table>
    

</div>

</div><!--panel panel-default--> 

</section><!--col-md-8-->

</section><!--col-md-10-->

<?php include("footer.php");?>