<?php session_start();
ob_start();
require_once('../convertvn.php');

if(!isset($_SESSION['adminuser'])){
	header("location:login.php");
}


include("../../db.php");

//Get Site Settings

if($SiteSettings = $mysqli->query("SELECT * FROM settings WHERE id='1'")){

  $Settings = mysqli_fetch_array($SiteSettings);
	
	$SiteLink = $Settings['siteurl'];
	
	$SiteSettings->close();
	
}else{
    
	?>
	<script>
		errorpage();
	</script>
	<?php
}

?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title><?php echo $Settings['name']; ?> - Admin</title>
<meta name="author" content="FlippyScripts">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=0">

<link href="<?php echo $Settings['datalink']; ?>/sysimg/favicon.ico" rel="shortcut icon" type="image/x-icon"/>

<link href="css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="css/style.css" rel="stylesheet" type="text/css">
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<script src="js/jquery.min.js"></script>	
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.menu.js"></script>
<script src="js/jquery.pjax.js" type="text/javascript"></script>

<script>
$(function(){
  $(document).pjax('a[target!="_blank"]', '.main-header');
});


</script>

</head>

<body>

<div id="fb-root"></div>
<script async defer src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.2"></script>


<div id="wrap">

<div class="container-fluid">

<header class="main-header" style="background: #dddddd;">

<a class="logo" href="index.php"><img class="img-responsive" src="<?php echo $Settings['datalink']; ?>/sysimg/logo.png" alt="Logo" style="height: 35px;"></a>

<a class="header-link pull-right" style="color: #454545;" href="logout.php">Đăng xuất</a>

</header>

<?php

//Convert UTC to GMT time
function utc_to_gmt($utctime) { 
  $unixtime = strtotime($utctime);
  $dateFormat="d-m-Y H:i:s"; //set the date format
  $timeshow=gmdate($dateFormat, $unixtime + 7 * 3600); //Time in Timezone GMT +7
  return $timeshow;
}

?>