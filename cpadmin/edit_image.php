<?php include("header.php");?>

<section class="col-md-2">

<?php include("left_menu.php");?>
                    
</section><!--col-md-2-->

<section class="col-md-10">

<ol class="breadcrumb">
  <li>Quản trị</li>
  <li>Bài đăng</li>
  <li>Ảnh</li>
  <li class="active">Sửa bài đăng</li>
</ol>

<div class="page-header">
  <h3>Sửa Ảnh <small>Sửa/ Thay đổi bài đăng</small></h3>
</div>

<script src="js/bootstrap-filestyle.min.js"></script>
<script>
$(function(){
$(":file").filestyle({iconName: "glyphicon-picture", buttonText: "Chọn Ảnh"});
});
</script>

<script type="text/javascript" src="js/jquery.form.js"></script>

<script>
$(document).ready(function()
{
    $('#photoUploader').on('submit', function(e)
    {
        e.preventDefault();
        $('#submitButton').attr('disabled', ''); // disable upload button
        //show uploading message
        $("#output").html('<div class="alert alert-info" role="alert">Đang tải lên ... Vui lòng chờ ...</div>');
		
        $(this).ajaxSubmit({
        target: '#output',
        success:  afterSuccess //call function after success
        });
    });
});
 
function afterSuccess()
{	
	 
    $('#submitButton').removeAttr('disabled'); //enable submit button
   
}
</script>

<section class="col-md-8">

<div class="panel panel-default">

    <div class="panel-body">
    
<?php

$id = $mysqli->escape_string($_GET['id']); 

if($Post = $mysqli->query("SELECT * FROM media WHERE id='$id'")){

    $PostRow = mysqli_fetch_array($Post);
	
	$SelectedCat = $PostRow['catid'];
	
    $Post->close();
	
}else{
    
	?>
	<script>
		errorpage();
	</script>
	<?php
}

?>    

<div id="output"></div>

<form id="photoUploader" action="update_image.php?id=<?php echo $id;?>" enctype="multipart/form-data" method="post">

<div class="form-group">
<label for="category">Chủ đề</label>
<select class="form-control" name="category" id="category">

<?php
if($CatSelected = $mysqli->query("SELECT * FROM categories WHERE id='$SelectedCat' LIMIT 1")){

($CatSelectedRow = mysqli_fetch_array($CatSelected));

$SelectedCat = $CatSelectedRow['id'];
		
?>   
	<option value="<?php echo $CatSelectedRow['id'];?>"><?php echo $CatSelectedRow['cname'];?></option>
<?php     
	
$CatSelected->close();

}else{
    ?>
	<script>
		errorpage();
	</script>
	<?php
}
?>

<?php
if($CatSql = $mysqli->query("SELECT * FROM categories WHERE id!='$SelectedCat' ORDER BY cname ASC")){

    while ($CatRow = mysqli_fetch_array($CatSql)){
		
?>   
	<option value="<?php echo $CatRow['id'];?>"><?php echo $CatRow['cname'];?></option>
<?php     
	}
$CatSql->close();
}else{
    ?>
	<script>
		errorpage();
	</script>
	<?php
}
?>
</select>
</div>

<div class="form-group">
<label for="inputTitle">Tiêu đề</label>
<input type="text" class="form-control" name="inputTitle" id="inputTitle" placeholder="Sửa tiêu đề" value="<?php echo $PostRow['title'];?>">
</div>
        
<div class="form-group">
<label for="inputfile">Thay đổi ảnh (tùy chọn)</label>
<input type="file" id="inputfile" name="inputfile" class="filestyle" data-iconName="glyphicon-picture" data-buttonText="Chọn Ảnh">
</div>

</div><!-- panel body -->

<div class="panel-footer clearfix">

<button type="submit" id="submitButton" class="btn btn-default btn-success btn-lg pull-right">Cập nhật</button>

</div><!--panel-footer clearfix-->

</form>


</div><!--panel panel-default-->  

</section>

</section><!--col-md-10-->

<?php include("footer.php");?>