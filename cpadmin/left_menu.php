<nav id="left-menu">
<ul>
   <li><a class="fa fa-dashboard" href="index.php"><span>Thông tin</span></a></li>
   <li class="has-sub"><a class="fa fa-file-text-o" href="javascript:void(0)"><span>Bài đăng</span></a>
   <ul>
         <li><a href="approved_pictures.php"><span>Ảnh đã duyệt</span></a></li>
         <li><a href="pending_pictures.php"><span>Ảnh chưa duyệt</span></a></li>
         <li><a href="approved_videos.php"><span>Video đã duyệt</span></a></li>
         <li><a href="pending_videos.php"><span>Video chưa duyệt</span></a></li>
      </ul>
   </li>
   <li><a class="fa fa-users" href="users.php"><span>Người dùng</span></a></li>
   <li><a class="fa fa-comments" href="comments.php"><span>Bình luận</span></a></li>
 
   <li class="has-sub"><a class="fa fa-th-large" href="javascript:void(0)"><span>Chủ đề</span></a>
      <ul>
         <li><a href="new_category.php"><span>Thêm chủ đề mới</span></a></li>
         <li><a href="manage_categories.php"><span>Quản lý chủ đề</span></a></li>
      </ul>
   </li>
   <li class="has-sub"><a class="fa fa-align-left" href="javascript:void(0)"><span>Quản lý trang</span></a>
      <ul>
         <li><a href="about.php"><span>Giới thiệu</span></a></li>
         <li><a href="privacy_policy.php"><span>Chính sách</span></a>
         <li><a href="tnc.php"><span>Nội quy</span></a>
         <li><a href="dmca.php"><span>Trang DMCA</span></a>
      </ul>
   </li>
   <li class="last"><a class="fa fa-eye" href="../" target="_blank"><span>Xem trang</span></a></li>
   <li><a class="fa fa-indent" href="ads.php"><span>Advertisements</span></a></li>
   <li><a class="fa fa-cogs" href="settings.php"><span>Settings</span></a></li>
   <li><a class="fa fa-user" href="administrator.php"><span>Administrator</span></a></li>
   
</ul>
</nav>