<?php 
include("header.php");

?>

<section class="col-md-2">

<?php include("left_menu.php");


if($SiteSettings = $mysqli->query("SELECT * FROM settings WHERE id='1'")){

    $SettingsRow = mysqli_fetch_array($SiteSettings);
	
    $SiteSettings->close();
	
}else{
    
	 printf("<div class='alert alert-danger alert-pull'>Đã xảy ra sự cố! Vui lòng thử lại!</div>");
}


?>
                    
</section><!--col-md-2-->

<section class="col-md-10">

<ol class="breadcrumb">
  <li>Admin CP</li>
  <li class="active">Settings</li>
</ol>

<div class="page-header">
  <h3>Site Settings <small>Update your website settings</small></h3>
</div>

<script src="js/bootstrap-filestyle.min.js"></script>
<script>
$(function(){
$(":file").filestyle({iconName: "glyphicon-picture", buttonText: "Select Photo"});
});
</script>

<script type="text/javascript" src="js/jquery.form.js"></script>

<script>
$(document).ready(function()
{
    $('#settingsFormOther').on('submit', function(e)
    {
        e.preventDefault();
        $('#submitButton').attr('disabled', ''); // disable upload button
        //show uploading message
        $("#output-other").html('<div class="alert alert-info" role="alert">Đang tải lên ... Vui lòng chờ ...</div>');
		
        $(this).ajaxSubmit({
        target: '#output-other',
        success:  afterSuccess //call function after success
        });
    });

    $('#settingsForm').on('submit', function(e)
    {
        e.preventDefault();
        $('#submitButton').attr('disabled', ''); // disable upload button
        //show uploading message
        $("#output").html('<div class="alert alert-info" role="alert">Đang tải lên ... Vui lòng chờ ...</div>');
		
        $(this).ajaxSubmit({
        target: '#output',
        success:  afterSuccess //call function after success
        });
    });
});
 
function afterSuccess()
{	
	 
    $('#submitButton').removeAttr('disabled'); //enable submit button
   
}
</script>

<section class="col-md-8">

<div class="panel panel-default">


<!-- Other Settings -->
<div class="panel-body">

<form id="settingsFormOther" action="update_settings_other.php" enctype="multipart/form-data" method="post">

<div class="form-group">
    <label for="inputSlogan">Website Slogan</label>
    <textarea class="form-control" id="inputSlogan" name="inputSlogan" rows="1" placeholder="Enter a meta description for your website Slogan"><?php echo $SettingsRow['slogan']?></textarea>
</div>

</div><!-- panel body -->

<div id="output-other"></div>

<div class="panel-footer clearfix">

    <button type="submit" id="submitButton" class="btn btn-default btn-success btn-lg pull-right">Cập nhật</button>

</div><!--panel-footer clearfix-->

</form>


<!-- System Settings -->
<div class="panel-body">

<form id="settingsForm" action="update_settings.php" enctype="multipart/form-data" method="post">

<div class="form-group">
        <label for="inputTitle">Website Title</label>
    <div class="input-group">
         <span class="input-group-addon"><span class="glyphicon fa  fa-info"></span></span>
      <input type="text" id="inputTitle" name="inputTitle" class="form-control" placeholder="ex: Vuikk" value="<?php echo $SettingsRow['name']?>">
    </div>
</div>

<div class="form-group">
        <!--label for="inputSiteurl">Website URL (Without "https://" and end "/")</label-->
        <label for="inputSiteurl">Website URL (Full)</label>
    <div class="input-group">
         <span class="input-group-addon"><span class="glyphicon fa  fa-info"></span></span>
      <input type="text" id="inputSiteurl" name="inputSiteurl" class="form-control" placeholder="ex: https://vuikk.com" value="<?php echo $SettingsRow['siteurl']?>">
    </div>
</div>

<div class="form-group">
<label for="inputDescription">Meta Description</label>
<textarea class="form-control" id="inputDescription" name="inputDescription" rows="1" placeholder="Enter a meta description for your website"><?php echo $SettingsRow['descrp']?></textarea>
</div>

<div class="form-group">
<label for="inputKeywords">Meta Keywords</label>
<textarea class="form-control" id="inputKeywords" name="inputKeywords" rows="1" placeholder="Enter a meta keywords for your website"><?php echo $SettingsRow['keywords']?></textarea>
</div>

<div class="form-group">
        <label for="inputEmail">Admin Email Address</label>
    <div class="input-group">
         <span class="input-group-addon">@</span>
      <input type="text" id="inputEmail" name="inputEmail" class="form-control" placeholder="Enter your website email address" value="<?php echo $SettingsRow['email']?>">
    </div>
</div>

<hr></hr>

<div class="row">

		 <div class="col-xs-6">
            <label for="inputOpenPosts">Open Posts in</label>
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-link"></span></span>
                    <select class="form-control" id="inputOpenPosts" name="inputOpenPosts">
 					<?php if ($SettingsRow['open_posts']==1){?>
					<option value="1">New Tab</option>
					<option value="0">Self (Same Window)</option>
					<?php }else{?>
					<option value="0">Self (Same Window)</option>
					<option value="1">New Tab</option>
					<?php }?>
					</select>
                </div>

            </div>
            
            <div class="col-xs-6">
            <label for="inputIdle">User Idle Pop-up</label>
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-clock-o"></span></span>
                    <select class="form-control" id="inputIdle" name="inputIdle">
                    <?php if(!empty($Settings['idle_popup'])){?>
                    <option value="<?php echo $Settings['idle_popup'];?>"><?php echo $Settings['idle_popup'];?> Minutes</option>
                    <?php }?>
					<option value="0">Off</option>
					<option value="3">3 Minutes</option>
                    <option value="5">5 Minutes</option>
                    <option value="10">10 Minutes</option>
					</select>
                </div>
            </div>
            
            
            <div class="col-xs-6">
            <label for="inputFbapp">Facebook App ID</label>
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-facebook-square"></span></span>
                    <input type="text" id="inputFbapp" name="inputFbapp" class="form-control" placeholder="Enter your Facebook app id" value="<?php echo $SettingsRow['fbapp']?>">
                </div>
            </div>
            
            
            <div class="col-xs-6">
            <label for="inputFbSecret">Facebook App Secret</label>
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-facebook-square"></span></span>
                    <input type="text" id="inputFbSecret" name="inputFbSecret" class="form-control" placeholder="Enter your Facebook app secret key" value="<?php echo $SettingsRow['fb_key']?>">
                </div>
            </div>


            <div class="col-xs-6">
            <label for="inputGgapp">Google App ID</label>
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-google"></span></span>
                    <input type="text" id="inputGgapp" name="inputGgapp" class="form-control" placeholder="Enter your Google app id" value="<?php echo $SettingsRow['ggapp']?>">
                </div>
            </div>

            <div class="col-xs-6">
            <label for="inputGgSecret">Google App Secret</label>
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-google"></span></span>
                    <input type="text" id="inputGgSecret" name="inputGgSecret" class="form-control" placeholder="Enter your Google app secret key" value="<?php echo $SettingsRow['gg_key']?>">
                </div>
            </div>

            <div class="col-xs-6">
            <label for="inputDataName">Cloud Data Name</label>
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-check"></span></span>
                    <input type="text" id="inputDataName" name="inputDataName" class="form-control" placeholder="Enter your Cloud data name" value="<?php echo $SettingsRow['dataname']?>">
                </div>
            </div>

            <div class="col-xs-6">
            <label for="inputDataKey">Cloud Data Key</label>
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-check"></span></span>
                    <input type="text" id="inputDataKey" name="inputDataKey" class="form-control" placeholder="Enter your Cloud data key" value="<?php echo $SettingsRow['data_key']?>">
                </div>
            </div>
                        
</div>

<hr></hr>

<div class="row">
                    
            <div class="col-xs-6">
            <label for="inputFbpage">Facebook Page URL</label>
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-facebook"></span></span>
                    <input type="text" id="inputFbpage" name="inputFbpage" class="form-control" placeholder="Facebook page url" value="<?php echo $SettingsRow['fbpage']?>">
                </div>
            </div>


            <div class="col-xs-6">
            <label for="inputGA">Google Analytics Code</label>
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-google"></span></span>
                    <input type="text" id="inputGA" name="inputGA" class="form-control" placeholder="Ex: UA-12345678-1" value="<?php echo $SettingsRow['ga_code']?>">
                </div>
            </div>

            <div class="col-xs-6">
            <label for="inputSendEmail">Send code Email</label>
                <div class="input-group">
                    <span class="input-group-addon">@</span>
                    <input type="text" id="inputSendEmail" name="inputSendEmail" class="form-control" placeholder="Email to send code" value="<?php echo $SettingsRow['sendemail']?>">
                </div>
            </div>

            <div class="col-xs-6">
            <label for="inputSendPass">Send code Pass</label>
                <div class="input-group">
                    <span class="input-group-addon">*</span>
                    <input type="text" id="inputSendPass" name="inputSendPass" class="form-control" placeholder="Email pw to send code" value="<?php echo $SettingsRow['sendpass']?>">
                </div>
            </div>

            <div class="col-xs-6">
            <label for="inputApprove">Auto Approve Posts</label>
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-check"></span></span>
                    <select class="form-control" id="inputApprove" name="inputApprove">
 					 <?php if ($SettingsRow['active']==1){?>
					<option value="1">ON</option>
					<option value="0">OFF</option>
					<?php }else{?>
					<option value="0">OFF</option>
					<option value="1">ON</option>
					<?php }?>
					</select>
                </div>
            </div>

            <div class="col-xs-6">
            <label for="inputDataLink">Data Link (without "/" at end)</label>
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-check"></span></span>
                    <input type="text" id="inputDataLink" name="inputDataLink" class="form-control" placeholder="Enter Data Link" value="<?php echo $SettingsRow['datalink']?>">
                </div>
            </div>

</div>

<hr></hr>

<div class="row">
            
            <div class="col-xs-6">
            <label for="inputLimitHome">Limit latest days for Home page</label>
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-check"></span></span>
                    <input type="number" id="inputLimitHome" name="inputLimitHome" class="form-control" min="1" placeholder="Enter days" value="<?php echo $SettingsRow['limit_home']?>">
                </div>
            </div>
            
            <div class="col-xs-6">
            <label for="inputLimitHot">Limit latest days for Hot and Trending page</label>
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-check"></span></span>
                    <input type="number" id="inputLimitHot" name="inputLimitHot" class="form-control" min="1" placeholder="Enter days" value="<?php echo $SettingsRow['limit_hot']?>">
                </div>
            </div>

            <div class="col-xs-6">
            <label for="inputLimitPoint">Limit points auto delete post</label>
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-check"></span></span>
                    <input type="number" id="inputLimitPoint" name="inputLimitPoint" class="form-control" max="-1" placeholder="Enter days" value="<?php echo $SettingsRow['limit_points']?>">
                </div>
            </div> 

            <div class="col-xs-6">
            <label for="inputLimitReport">Limit reports auto delete post</label>
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-check"></span></span>
                    <input type="number" id="inputLimitReport" name="inputLimitReport" class="form-control" min="1" placeholder="Enter days" value="<?php echo $SettingsRow['limit_reports']?>">
                </div>
            </div> 

            <div class="col-xs-6">
            <label for="inputLimitDelete">Limit latest days to delete post</label>
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-check"></span></span>
                    <input type="number" id="inputLimitDelete" name="inputLimitDelete" class="form-control" min="7" <?php if(!empty($Settings['limit_delete'])){?> value="<?php echo $SettingsRow['limit_delete']?>" <?php } else{ ?> value="9999" <?php }?>>
                </div>
            </div>
            
            <div class="col-xs-6">
            <label for="inputLimitPost">Limit number of posts per day</label>
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-check"></span></span>
                    <input type="number" id="inputLimitPost" name="inputLimitPost" class="form-control" min="3" placeholder="Enter days" value="<?php echo $SettingsRow['limit_posts']?>">
                </div>
            </div> 

</div><!--row-->

</div><!-- panel body -->

<div id="output"></div>

<div class="panel-footer clearfix">

<button type="submit" id="submitButton" class="btn btn-default btn-success btn-lg pull-right">Update Site Settings</button>

</div><!--panel-footer clearfix-->

</form>


</div><!--panel panel-default-->  

</section>

</section><!--col-md-10-->

<?php include("footer.php");?>