<?php include("header.php");?>

<section class="col-md-2">

<?php include("left_menu.php");?>
                    
</section><!--col-md-2-->

<section class="col-md-10">

<ol class="breadcrumb">
  <li>Quản trị</li>
  <li>Chủ đề</li>
  <li class="active">Thêm chủ đề mới</li>
</ol>

<div class="page-header">
  <h3>Chủ đề mới <small>Thêm chủ đề mới</small></h3>
</div>

<script type="text/javascript" src="js/jquery.form.js"></script>

<script>
$(document).ready(function()
{
    $('#categoryForm').on('submit', function(e)
    {
        e.preventDefault();
        $('#submitButton').attr('disabled', ''); // disable upload button
        //show uploading message
        $("#output").html('<div class="alert alert-info" role="alert">Đang tải lên ... Vui lòng chờ ...</div>');
		
        $(this).ajaxSubmit({
        target: '#output',
        success:  afterSuccess //call function after success
        });
    });
});
 
function afterSuccess()
{	
	 
    $('#submitButton').removeAttr('disabled'); //enable submit button
   
}
</script>

<section class="col-md-8">

<div class="panel panel-default">

    <div class="panel-body">

<div id="output"></div>

<form id="categoryForm" action="submit_category.php" method="post">

<div class="form-group">
        <label for="inputTitle">Chủ đề</label>
    <div class="input-group">
         <span class="input-group-addon"><span class="glyphicon fa  fa-info"></span></span>
      <input type="text" id="inputTitle" name="inputTitle" class="form-control" placeholder="Nhập tên chủ đề" >
    </div>
</div>


<div class="form-group">
<label for="inputDescription">Mô tả</label>
<textarea class="form-control" id="inputDescription" name="inputDescription" rows="3" placeholder="Nhập mô tả"></textarea>
</div>





<button type="submit" id="submitButton" class="btn btn-default btn-success btn-lg pull-right">Xác nhận</button>


</form>

</div><!-- panel body -->

</div><!--panel panel-default-->  

</section>

</section><!--col-md-10-->

<?php include("footer.php");?>