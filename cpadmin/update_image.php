<?php

include('../../db.php');
include('../convertvn.php');

$id = $mysqli->escape_string($_GET['id']);

//Get Photo Info
if($Image = $mysqli->query("SELECT * FROM media WHERE id='$id'")){

    $ImageRow = mysqli_fetch_array($Image);
	
	$ImageFile = $ImageRow['image'];
	
    $Image->close();
	
}else{
    
	 printf("Error: %s\n", $mysqli->error);
}


//Data lake
require_once '../datalake/vendor/autoload.php';
use MicrosoftAzure\Storage\Blob\BlobRestProxy;
use MicrosoftAzure\Storage\Common\Exceptions\ServiceException;
use MicrosoftAzure\Storage\Blob\Models\CreateBlockBlobOptions;
//Access API
$ACCOUNT_NAME      = $Settings['dataname'];
$ACCOUNT_KEY       = $Settings['data_key'];
$connectionString  = "DefaultEndpointsProtocol=https;AccountName=".$ACCOUNT_NAME.";AccountKey=".$ACCOUNT_KEY;
$blobClient        = BlobRestProxy::createBlobService($connectionString); // Create blob client.
$containerName     = "data/uploads"; //Upload container
$options           = new CreateBlockBlobOptions();   //File type

if($_POST)
{	
	if(!isset($_POST['category']) || strlen($_POST['category'])<1)
	{
		//required variables are empty
		die('<div class="alert alert-danger">Vui lòng chọn chủ đề!</div>');
	}

	if(!isset($_POST['inputTitle']) || strlen($_POST['inputTitle'])<1)
	{
		//required variables are empty
		die('<div class="alert alert-danger" role="alert">Vui lòng nhập tiêu đề!</div>');
	}

	//FCatagory and file title and File name
	$Catagory			= $mysqli->escape_string($_POST['category']);
	$uploadTitle	= $mysqli->escape_string($_POST['inputTitle']); // file title
	$FileTitle		= mb_ucfirst($uploadTitle); //File Title will be used as new File name
	
	$strFileTitle	= strlen($FileTitle);
	if ($strFileTitle > 20) {
		$FileName	= convertvn(substr($FileTitle,0,20));
	}else{
		$FileName	= convertvn($FileTitle);
	}


if(isset($_FILES['inputfile']))
{

	if($_FILES['inputfile']['error'])
	{
		//File upload error encountered
		die('<div class="alert alert-danger">Đã xảy ra sự cố. Vui lòng thử lại!</div>');
	}
	
	//File property
	$file 					= $_FILES['inputfile']['tmp_name']; 
    $sourceProperties		= getimagesize($file);     
	list($width, $height) 	= $sourceProperties;
	$imageType 				= $sourceProperties[2];

	
	switch ($imageType) {
		//allowed file types
		case IMAGETYPE_JPEG:
			$Type = "1";
			if($width > 600){
				$imageResourceId = imagecreatefromjpeg($file); 
				$targetLayer = imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
				ob_start(); // start a new output buffer
  					imagejpeg($targetLayer);
					$imagedata = base64_encode(ob_get_clean());
				$content = fopen("data:image/jpg;base64,$imagedata", "r"); 	//data lake file content
			}else{
				$content = fopen($file, "r"); //data lake file content
			}
			$options->setContentType("image/jpg"); //data lake file type
			$ext = "jpg";
			break;

		case IMAGETYPE_PNG:
			$Type = "1";
			if($width > 600){
				$imageResourceId = imagecreatefrompng($file); 
				$targetLayer = imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
				ob_start(); // start a new output buffer
					imagejpeg($targetLayer);
					$imagedata = base64_encode(ob_get_clean());
				$content = fopen("data:image/jpg;base64,$imagedata", "r"); 	//data lake file content
			}else{
				$content = fopen($file, "r"); //data lake file content
			}
			$options->setContentType("image/jpg"); //data lake file type
			$ext = "jpg";
			break;

		case IMAGETYPE_GIF:
			$Type = "2";
			if( filesize($file) > 3 * 1048576 ){
				die('<div class="alert alert-danger">Vui lòng chọn ảnh động kích thước nhỏ hơn 3M.</div>');
			}else{
				$content = fopen($file, "r");
			}
			$options->setContentType("image/gif"); //data lake file type
			$ext = "gif";
			break;
		
		default:
			die('<div class="alert alert-danger">Chọn sai định dạng. Vui lòng chọn lại ảnh!</div>'); //output error
			break;
	}
	
  	//Rename and save uploded file to destination folder.
   	if($Type == 1 || $Type == 2)
   	{
	//Name catagory and post date
	$NewFileName = $mysqli->escape_string(date("Ymd")."_".uniqid()."_".$FileName. ".". $ext);
	
	
	try {
		//Upload new image to data lake
		$blobClient->createBlockBlob($containerName, $NewFileName, $content, $options);

		if(!empty($ImageFile)){
		//Delete old image
		$blobClient->deleteBlob($containerName, $ImageFile);
		}
	}
	catch(ServiceException $e){
		$code = $e->getCode();
		$error_message = $e->getMessage();
	}
	
	// Update info into database
	$mysqli->query("UPDATE media SET title='$FileTitle',catid='$Catagory',type='$Type',image='$NewFileName' WHERE id='$id'");
		
	// Free up memory
	unset($targetLayer);
	unset($imageResourceId);
	unset($imagedata);
	unset($content);		
	
	}else{
		die('<div class="alert alert-danger">Đã xảy ra sự cố. Vui lòng thử lại!</div>');
	}

}else{
	   
	$mysqli->query("UPDATE media SET title='$FileTitle',catid='$Catagory' WHERE id='$id'");
	   
 }

	die('<div class="alert alert-success" role="alert">Cập nhật xong.</div>');

		
   }else{
	   
   		die('<div class="alert alert-danger" role="alert">Đã xảy ra sự cố. Vui lòng thử lại!</div>');
   }


//Function to resize image
function imageResize($imageResourceId,$width,$height) {

    $targetWidth = 600;
    $targetHeight = round($targetWidth * $height / $width);

    $targetLayer=imagecreatetruecolor($targetWidth,$targetHeight);
    imagecopyresampled($targetLayer,$imageResourceId,0,0,0,0,$targetWidth,$targetHeight, $width,$height);

	return $targetLayer;   
}
?>