<body>

<div id="fb-root"></div>
<script async defer src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.2"></script>

<div id="wrap">
<nav id="myNavbar" class="navbar navbar-default navbar-fixed-top navbar-static-top custom-navbar" role="navigation">
  <div class="container-fluid">
    <div class="container"> 
      <!-- Brand and toggle get grouped for better mobile display -->
      <div type="button" class="navbar-toggle collapsed menubtn" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <div class="hamburger"></div>
      </div>

      <div class="navbar-header">
        <a class="navbar-brand" href="."><img src="<?php echo $Settings['datalink']; ?>/sysimg/logo.png" class="logo navbar-brand-centered" alt="logo"></a> 
      </div>
      <!-- navbar-header -->

      <!-- Non-collapsing right-side icons -->        
      <div class="nav navbar-nav navbar-right dropdown">
        <?php if(!isset($_SESSION['useremail']) || $Uid == 0){?>
        <li>
          <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="modal" data-target="#myModal" style="padding-top: 5px!important;"><img src="<?php echo $Settings['datalink']; ?>/sysimg/user-icon.svg"><span id="login-button">&nbsp; Đăng nhập</span></a>
        </li>
        <?php }else{ ?>
              <?php
              //get user link
              $UsLink = convertvn($Uname);
          
              ?>
        
        <a href="javascript:void(0)" class="btn btn-secondary dropdown-toggle" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            
            <?php if (empty($Uavatar)){ ?>    
              <img id="avatar-img" src="<?php echo $Settings['datalink']; ?>/sysimg/default-avatar.png" alt="" class="avatar-profile" style="object-fit: cover;" />
            <?php }elseif (substr($Uavatar,0,4) == "http"){?>
              <img id="avatar-img" src="<?php echo $Uavatar; ?>" alt="" class="avatar-profile" style="object-fit: cover;" />
            <?php }else{ ?>
              <img id="avatar-img" src="<?php echo $Settings['datalink']; ?>/avatars/<?php echo $Uavatar;?>" alt="" class="avatar-profile" style="object-fit: cover;" />
            <?php }?>
            
            <span id="login-id"><?php echo $Uname."&nbsp; ";?><span class="caret"></span></span></a>
            
            <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="dropdownMenuLink">
              <li role="presentation"><a role="menuitem" tabindex="-1" href="profile-<?php echo $Uid;?>-<?php echo $UsLink;?>.html"><i class="glyphicon glyphicon-user"></i>&nbsp; Trang cá nhân</a>
              <li class="divider"></li>
              <li role="presentation"><a role="menuitem" tabindex="-1" href="settings.html"><i class="glyphicon glyphicon-cog"></i>&nbsp; Cài đặt</a>
              <li class="divider"></li>
              <li role="presentation"><a role="menuitem" tabindex="-1" href="logout.html"><i class="glyphicon glyphicon-log-out"></i>&nbsp; Đăng xuất</a>
            </ul>
          <?php }?>
        
        </div>
     


      <!-- the collapsing menu -->
      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse navbar-left" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
          <li><a id="searchbtn" href="javascript:void(0)" onclick="searchbtn()"><i class="glyphicon glyphicon-search">&nbsp;</i>Tìm kiếm</a></li>
          <li><a href="."><i class="glyphicon glyphicon-home menuicon">&nbsp;</i>Trang chủ</a></li>
          <li><a href="hot.html"><i class="glyphicon glyphicon-heart-empty menuicon">&nbsp;</i>Nổi bật</a></li>
          <li><a href="trending.html"><i class="glyphicon glyphicon-star-empty menuicon">&nbsp;</i>Thịnh hành</a></li>
          <li><a href="fresh.html"><i class="glyphicon glyphicon-flash menuicon">&nbsp;</i>Mới nhất</a></li>
          <li><a href="video.html"><i class="glyphicon glyphicon-film menuicon">&nbsp;</i>Video</a></li>
          <li class="dropdown"><a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Chủ đề <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu" style="overflow: scroll;">
              <li><a href="gif.html">Ảnh động</a></li>
              <?php
              if($CatSql = $mysqli->query("SELECT id, cname FROM categories ORDER BY cname ASC")){

                  while($CatRow = mysqli_fetch_array($CatSql)){
    
                    $CatName = $CatRow['cname'];
                    $CatUrl = convertvn($CatName);    	
		                $CatDisplayName = $CatName;
		
		              ?>
                  <li><a href="category-<?php echo $CatRow['id'];?>-<?php echo $CatUrl;?>-1.html"><?php echo $CatDisplayName;?></a></li>
                  <?php
                  }

	                $CatSql->close();
	
              }else{
	                 ?><script>errorpage();</script><?php
              }?>
            </ul>
          </li>
        </ul>
      </div>
      <!-- /.navbar-collapse --> 
    </div>
    <!-- /.container -->   
  </div>
  <!-- /.container-fluid --> 
</nav>


<div>
  <button onclick="scrollWin()" id="myBtn" class="btn btn-default btn-sm" title="Lên đầu trang"><i class="fas fa-chevron-up"></i></span></button>
</div>




<!-- Account src -->

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content" align="center">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 class="modal-title" id="myModalLabel" style="font-weight:bold; font-size:20px;">Đăng nhập</h3>
      </div>
      <!-- /.modal-header -->
      
      <div class="modal-body">

        <div style="width:80%;">
          <!--facebook_button-->
          <a href="config_fb.php" class="fb-login"><img src="<?php echo $Settings['datalink']; ?>/sysimg/facebook-btn.svg" height="25"> &nbsp; Đăng nhập với Facebook</a>
          <!--google_button-->
          <a href="config_gg.php" class="gg-login"><img src="<?php echo $Settings['datalink']; ?>/sysimg/google-btn.svg" height="25"> &nbsp; Đăng nhập với Google</a>
          <br>
        </div>
        
        <form role="form" action="submit_login.php" id="FromLogin" method="post" style="width:90%">
          <div class="strike">
            <span>Đăng nhập bằng tài khoản<span>
          </div>
          <div id="output-login"></div>
          <div class="form-group">
            <!--div class="input-group"!-->
              <input type="text" class="form-control" name="uEmail" id="uEmail" placeholder="Email">
              <!--label for="username" class="input-group-addon glyphicon glyphicon-user"!-->
            <!--/div!-->
          </div>
          <!-- /.form-group -->
          
          <div class="form-group">
            <!--div class="input-group"!-->
              <input type="password" class="form-control" name="password" id="password" placeholder="Mật khẩu">
              <!--label for="password" class="input-group-addon glyphicon glyphicon-lock"></label!-->
            <!--/div!-->
            <!-- /.input-group --> 
          </div>
          <!-- /.form-group -->
          
          <div style="text-align:left"> <a href="recover.html">Quên mật khẩu?</a> </div>
          <!-- /.checkbox -->
          
          <div class="modal-footer">
            <button type="submit" class="form-control btn btn-primary submitButton">Đăng nhập</button>
          </div>
          <!-- /.modal-footer -->

          <div id="registerUser"><a data-toggle="modal" data-target="#modalRegister" href="javascript:void(0)">Thành viên mới?<span style="font-weight:bold;"> <u>Đăng ký!</u></span></a></div>
          
        </form>
      </div>
      <!-- /.modal-body --> 
      
    </div>
    <!-- /.modal-content --> 
  </div>
  <!-- /.modal-dialog --> 
</div>
<!-- /.modal --> 

<!-- /.Model-Register -->
<div class="modal fade" id="modalRegister" tabindex="-1" role="dialog" aria-labelledby="myModalLabel-3" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content" align="center">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title" id="myModalLabel-3" style="font-weight:bold; font-size:20px">Đăng ký</h4>
      </div>
      <!-- /.modal-header -->
      
      <div class="modal-body">
        <div style="width:80%;">
          <a href="config_fb.php" class="fb-login"><img src="<?php echo $Settings['datalink']; ?>/sysimg/facebook-btn.svg" height="25"> &nbsp; Đăng ký với Facebook</a>
          <a href="config_gg.php" class="gg-login"><img src="<?php echo $Settings['datalink']; ?>/sysimg/google-btn.svg" height="25"> &nbsp; Đăng ký với Google</a>
        </div>

        <form action="submit_user.php" id="FromRegister" method="post" style="width:90%;">
          <div class="strike">
            <span>Hoặc<span>
          </div>
          <div id="output-register"></div>
          <div class="form-group" >
            <div align="left"><label for="uName">Tên hiển thị</label></div>
            <input type="text" class="form-control" name="uName" id="uName" placeholder="VD: Tôn Ngộ Không"/>
          </div>
          <div class="form-group">
            <div align="left"><label for="uEmail">Email đăng nhập</label></div>
            <input type="text" class="form-control" name="uEmail" id="uEmail" placeholder="Nhập địa chỉ email"/>
          </div>
          <div class="form-group">
            <div align="left"><label for="uPassword">Mật khẩu</label></div>
            <input type="password" class="form-control" name="uPassword" id="uPassword" placeholder="6 ký tự trở lên"/>
          </div>
          <div class="form-group">
            <div align="left"><label for="cPassword">Xác nhận</label></div>
            <input type="password" class="form-control" name="cPassword" id="cPassword" placeholder="Nhập lại mật khẩu"/>
          </div>
          <div class="modal-footer">
            <button class="form-control btn btn-primary submitButton">Đăng ký</button>
          </div>
          <!-- /.modal-footer -->
          
        </form>
      </div>
      <!-- /.Model-Register --> 
      <!-- /.modal-body --> 
      
    </div>
    <!-- /.modal-content --> 
  </div>
  <!-- /.modal-dialog --> 
</div>

<!-- /.Model-Activate -->
<div class="modal fade" id="myActivate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel-5" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content" align="center">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 class="modal-title" id="myModalLabel-5" style="font-weight:bold; font-size:20px;">Kích hoạt tài khoản</h3>
      </div>
      <!-- /.modal-header -->

      <div class="modal-body" style="width:80%;">
        <div id="output-activate"></div>
        <div>
          <p>Nhập mã kích hoạt đã gửi tới email<br></p>
        </div>
        <div id="output-email" style="font-weight: bold;"></div>
        <br>

        <!-- Verify activate code form-->
        <form role="form" action="submit_activate.php" id="FromActivate" method="post">
          <div>
            <input type="text" class="form-control" name="uActivate" id="uActivate" pattern="\d*" style="text-align: center; width: 50%;" placeholder="Mã kích hoạt">
          </div>
          <!-- /.form-group -->
          <br>
          <br>
          <!-- /.checkbox -->
          
          <div class="modal-footer">
            <button type="submit" class="form-control btn btn-primary submitButton">Kích hoạt</button>
          </div>
          <!-- /.modal-footer -->
        </form>
        
        <form role="form" action="send_again.php" id="FromResend">
          <div style="text-align: left;"><button class="btn btn-link" id="resendButton">Gửi lại mã kích hoạt</u></a><span id="ctimer"></span></button></div>
        </form>
      </div>
      <!-- /.modal-body --> 
    </div>
    <!-- /.modal-content --> 
  </div>
  <!-- /.modal-dialog --> 
</div>
<!-- /.modal --> 



<div class="modal fade" id="modelAlert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel-6" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content" align="center">      
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 class="modal-title" id="myModalLabel-6" style="font-weight:bold; font-size:20px;">Thông báo</h3>
      </div>
      <!-- /.modal-header -->
      <br>
      <div>
        <h3>Bạn đã dùng hết <?php echo $LimitPost; ?> lượt đăng bài trong ngày!</h3>
        <span>Mỗi bài đăng đạt trên 100 lượt thích sẽ được tặng 1 lượt đăng.</span>
        <br><br>
        <h3>Cảm ơn bạn đã ủng hộ cộng đồng <?php echo $Settings['name']; ?>!</h3>
      </div>
      <br>          
      <div class="modal-footer">
        <div class="col text-center">
        <button style="width: 250px;" type="button" class="btn btn-primary" data-dismiss="modal" aria-hidden="true">OK</button>
        </div>
      </div>
      <!-- /.modal-body --> 
    </div>
    <!-- /.modal-content --> 
  </div>
  <!-- /.modal-dialog --> 
</div>
<!-- /.modal -->


<!-- Modal-Submit_images -->
<div class="modal fade" id="modelImages" tabindex="-1" role="dialog" aria-labelledby="modalLabel-1" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title" id="modalLabel-1">Đăng ảnh vui</h4>
      </div>
      <!-- /.modal-header -->
      
      <div class="modal-body">
        <div class="info">Đăng những hình ảnh gây cười, hấp dẫn!</div>
        <br>      
        <form action="submit_images.php" id="FileUploader" enctype="multipart/form-data" method="post" >

          <div class="form-group">
            <label for="mFile">Ảnh (gif, jpg, png)</label>
            <input type="file" name="mFile" id="mFile" accept="image/*" />
          </div>

          <div style="text-align: center; margin-bottom: 15px; overflow: hidden;">
            <img id="output_media" style="display: none; width: 200px; height: auto;" />
          </div>

          <div class="form-group">
            <label for="catagory-select">Chủ đề</label>
            <select name="catagory-select" id="catagory-select" class="form-control">
              <option value="">Chọn chủ đề</option>
              <?php
              if($CatSelect = $mysqli->query("SELECT id, cname FROM categories ORDER BY cname ASC")){

                    while($CatsRow = mysqli_fetch_array($CatSelect)){
				
                      ?>
                        <option value="<?php echo $CatsRow['id'];?>"><?php echo $CatsRow['cname'];?></option>
                      <?php

                    }

	                  $CatSelect->close();
	
              }else{
    
                  ?>
                  <script>errorpage();</script>
	                <?php
              }
              ?>
            </select>
          </div>

          <div class="form-group">
            <label for="mName">Tiêu đề</label>
            <textarea class="form-control" name="mName" id="mName" rows='1' placeholder="Nhập tiêu đề"></textarea>
          </div>
          
          <div id="output-image" style="text-align:center;"></div>
          <div class="col text-center">
            <input type="button" class="btn btn-default" onclick="resetMediaForm()" value="Hủy" style="margin-right: 5px;" />
            <input type="submit" class="btn btn-primary submitButton" value="Đăng bài" />
          </div>
          <!-- /.modal-footer -->
          
        </form>
      </div>
      <!-- /.modal-body --> 
      
    </div>
    <!-- /.modal-content --> 
  </div>
  <!-- /.modal-dialog --> 
</div>
<!-- Modal-Submit_images --> 

<!-- Modal-Submit-Videos -->
<div class="modal fade" id="modelVideos" tabindex="-1" role="dialog" aria-labelledby="modalLabel-2" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title" id="modalLabel-2">Đăng video vui</h4>
      </div>
      <!-- /.modal-header -->
      
      <div class="modal-body">
        <div class="info">Đăng những video gây cười, hấp dẫn!</div>
        <br>
        <form action="submit_videos.php" id="VideoSubmit" method="post">
          
          <div class="form-group">
            <label for="mLink">Video (nguồn Youtube/ Facebook/) </label>
            <input type="text" name="mLink" id="mLink" class="form-control" placeholder="Dán liên kết video"/>
          </div>

          <div class="form-group">
            <label for="catagory-select-2">Chủ đề</label>
            <select name="catagory-select-2" id="catagory-select-2" class="form-control">
              <option value="">Chọn chủ đề</option>
              <?php
              if($CatSelect2 = $mysqli->query("SELECT id, cname FROM categories ORDER BY cname ASC")){

              while($CatsRow2 = mysqli_fetch_array($CatSelect2)){ ?>
                <option value="<?php echo $CatsRow2['id'];?>"><?php echo $CatsRow2['cname'];?></option>
              <?php
              }

	            $CatSelect2->close();
	
              } else{ ?>

	<script>
		errorpage();
	</script>
	<?php
}
?>
            </select>
          </div>
          <div class="form-group">
            <label for="vName">Tiêu đề</label>
            <textarea class="form-control" name="vName" id="vName" rows='1' placeholder="Nhập tiêu đề"></textarea>
          </div>
          
          <div id="output-video" style="text-align:center;"></div>
          <div class="col text-center">
            <input type="button" class="btn btn-default" onclick="$('#modelVideos').modal('hide'); $('#VideoSubmit').resetForm();" value="Hủy" style="margin-right: 5px;" />
            <input type="submit" class="btn btn-primary submitButton" value="Đăng bài" />
          </div>
          <!-- /.modal-footer -->
          
        </form>
      </div>
      <!-- /.modal-body --> 
      
    </div>
    <!-- /.modal-content --> 
  </div>
  <!-- /.modal-dialog --> 
</div>
<!-- Modal-Submit-Videos --> 


<!-- Report button -->
<div class="modal fade" id="myReport" tabindex="-1" role="dialog" aria-labelledby="myModalLabel-7" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" align="center">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title" id="myModalLabel-7" style="font-weight:bold; font-size:16px;">Báo cáo vi phạm</h4>

            </div>
            <div class="modal-body">
                <h3>Nhấn "Xác nhận" để báo cáo nội dung xấu hoặc chưa phù hợp.</h3>
                <h3>Nhấn "Hủy" để trở lại.</h3>
            </div>
            <!--/modal-body-collapse -->
            <div class="modal-footer">
              <div class="col text-center">
                <input type="button" class="btn btn-default" data-dismiss="modal" value="Hủy" />
                <input type="button" class="btn btn-danger" id="btnReportYes" value="Xác nhận" onclick="submitreportpost()" />
              </div>
            </div>
            <!--/modal-footer-collapse -->
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<!-- Delete button -->
<div class="modal fade" id="myDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel-8" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" align="center">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title" id="myModalLabel-8" style="font-weight:bold; font-size:16px;">Xóa bài đăng</h4>

            </div>
            <div class="modal-body">
                <h3>Bạn có chắc chắn muốn Xóa bài đăng?</h3>
                <p><small>Nhấn "Xóa" để xác nhận. Nhấn "Hủy" để trở lại.</small></p>
                <p class="text-danger"><small>Nội dung không thể khôi phục sau khi xóa.</small></p>
            </div>
            <!--/modal-body-collapse -->
            <div class="modal-footer">
              <div class="col text-center">
                <input type="button" class="btn btn-default" data-dismiss="modal" value="Hủy" />
                <input type="button" class="btn btn-danger" id="btnDeleteYes" value="Xóa" style="width: 70px;" onclick="submitdeletepost()" />
              </div>
            </div>
            <!--/modal-footer-collapse -->
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<!-- Modal-Edit_Post -->
<div class="modal fade" id="modelEditPost" tabindex="-1" role="dialog" aria-labelledby="modalLabel-9" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title" id="modalLabel-9">Sửa bài đăng</h4>
      </div>
      <!-- /.modal-header -->
      
      <div class="modal-body" id="media_detail"></div>
      <!-- /.modal-body --> 
      
    </div>
    <!-- /.modal-content --> 
  </div>
  <!-- /.modal-dialog --> 
</div>
<!-- Modal-Edit --> 
