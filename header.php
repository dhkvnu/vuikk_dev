<?php session_start();
include('header_php.php');
?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title><?php echo $pageTitle; ?></title>
<meta name="description" content="<?php echo $Settings['descrp']; ?>" />
<meta name="keywords" content="<?php echo $Settings['keywords']; ?>" />

<!--Facebook Meta Tags-->
<meta property="fb:app_id"          content="<?php echo $Settings['fbapp']; ?>" />
<meta property="og:url"             content="<?php echo $Settings['siteurl']; ?>" />
<meta property="og:title"           content="<?php echo $Settings['name']; ?>" />
<meta property="og:description" 	  content="<?php echo $Settings['descrp']; ?>" />
<meta property="og:image"           content="<?php echo $Settings['datalink']; ?>/sysimg/logo.png" />
<!--End Facebook Meta Tags-->


<!--- Require php file --->
<?php 
include('header_js.php');
?>

</head>

<!--- Require php file for menu bar--->
<?php 
include('header_div.php');
?>

<!-- Title line -->
<div class="bottom-header">
  <div class="container">
    <div class="header-bottom-left">
      <div id="slogan"><?php echo $Settings['descrp']; ?></div>
      <div id="social-love">
        <div id="fb-button-div">
          <div class="fb-like" data-href="<?php echo $Settings['fbpage'];?>" data-width="50px" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>
        </div>
      </div>
      <!--/.social-love -->     
    </div>
    <!--/.header-bottom-left -->

    <div id="search-box">
      <form class="navbar-form" role="search" id="search" method="get" action="search.php">
        <div class="input-group">
          <input type="text" class="form-control" placeholder="Tìm kiếm" id="term" name="term">
          <div class="input-group-btn">
            <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
          </div>
        </div>
      </form>
    </div>
  </div>
  <!--center-header--> 
</div>
<!--bottom-header-->

<!-- end -->