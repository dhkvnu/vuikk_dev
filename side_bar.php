<?php if(!empty($Ad1)){ ?>
	<div class="ads-one">
		<?php echo $Ad1; ?>
	</div>
<?php }?>

<div id="featured-box">
<div class="right-bar-title"><h3><b>Đề xuất</b></h3></div>

<?php

if ($Featured = $mysqli->query("SELECT * FROM media WHERE active=1 ORDER BY RAND() LIMIT 4")){
	while($FeaturedRow = mysqli_fetch_array($Featured)){
		
	$FeaturedType = $FeaturedRow['type']; 		
	$FeaturedTitle= stripslashes($FeaturedRow['title']);
	$FeaturedMediaTitle = short_title($FeaturedTitle);
	
	$FeaturedLink = convertvn($FeaturedMediaTitle);
	$FeaturedLink = nl2br($FeaturedLink);
?>
<a href="post-<?php echo $FeaturedRow['id'];?>-<?php echo $FeaturedLink;?>.html" <?php if ($MediaOpen==1){?> target="_blank" <?php }?>>
	<div class="featured-post">
		<?php if($FeaturedType=='1'){?>

			<img class="featured-imgBar" alt="" src="<?php echo $Settings['datalink']; ?>/uploads/<?php echo $FeaturedRow['image'];?>">
		
		<?php } elseif($FeaturedType=='2'){ ?>

<div style="width: 300px; height: 150px; overflow: hidden;">

	<div class="gif-img"><span class="glyphicon glyphicon-picture"></span> GIF</div>
	<img alt="" data-gifffer="<?php echo $Settings['datalink']; ?>/uploads/<?php echo $FeaturedRow['image'];?>" style="width: 300px;">

</div>
	
<?php } else{?>
			<?php if ($FeaturedRow['video_type'] == "youtube") { ?>

				<div class="play-button"><span class="glyphicon glyphicon-facetime-video"></span>&nbsp; Video</div>
				<img class="featured-imgBar" alt="" src="https://img.youtube.com/vi/<?php echo $FeaturedRow['video_id'];?>/hqdefault.jpg">

			<?php } else if ($FeaturedRow['video_type'] =="facebook") { ?>
				<div style="width: 300px; height: 150px;">
					<div class="play-button"><span class="glyphicon glyphicon-facetime-video"></span>&nbsp; Video</div>
					<div class="video-frame-sidebar"></div>
					<div class="fb-video" data-href="https://www.facebook.com/facebook/videos/<?php echo $FeaturedRow['video_id']; ?>/" data-show-text="false" data-width="300" data-height="150" data-controls="false"></div>
				</div>
<?php } }?>
			<div class="featured-title"><h3><?php echo $FeaturedMediaTitle;?></h3></div>
	</div><!--featured-post-->
</a>
<?php     
	}
$Featured->close();
}else{
    ?>
	<script>
		errorpage();
	</script>
	<?php
}
?>
</div><!--featured-box-->

<?php if(!empty($Ad2)){ ?>
	<div id="ads-two">
		<?php echo $Ad2; ?>
	</div>
<?php }?>
