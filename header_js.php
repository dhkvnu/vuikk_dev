<!-- <head> -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=0">
<link href="<?php echo $Settings['datalink']; ?>/sysimg/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css" rel="stylesheet" type="text/css">
<link href="temp/css/style.css" rel="stylesheet" type="text/css">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.3.0/jquery.form.js" type="text/javascript"></script>
<script src="js/jquery.ui.shake.js" type="text/javascript"></script>
<script src="js/bootstrap-filestyle.min.js"></script>
<script type="text/javascript" src="js/gifffer.min.js"></script>
<script src="js/jquery.idle.min.js"></script>

<!--Google Services-->
<script data-ad-client="ca-pub-7048659988587499" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>


<script type="text/javascript">
function popup(e){var t=700;var n=400;var r=(screen.width-t)/2;var i=(screen.height-n)/2;var s="width="+t+", height="+n;s+=", top="+i+", left="+r;s+=", directories=no";s+=", location=no";s+=", menubar=no";s+=", resizable=no";s+=", scrollbars=no";s+=", status=no";s+=", toolbar=no";newwin=window.open(e,"windowname5",s);if(window.focus){newwin.focus()}return false}


$(function() {

$(":file").filestyle({iconName: "glyphicon-picture", buttonText: "Chọn Ảnh"});

});

//Show more text for too long title
function showtext(caller) {
  var id = $(caller).attr('data-id');
  $('#view-more-' + id).hide();
  $('#more-text-' + id).show();
}


//Media post button check
function postMedia(caller) {
  var maxpost = parseInt($(caller).attr('data-post'));
  var name    = $(caller).attr('data-name');

  if(maxpost > 0) {
    if(name == "image") {
      $('#modelImages').modal('show');
    } else {
      $('#modelVideos').modal('show');
    }
  } else {
    $('#modelAlert').modal('show');
  }
}


//Reset media post
function resetMediaForm(){
  $('#modelImages').modal('hide');
  $('#FileUploader').resetForm();
  $('#output_media').attr('src', '');
  $('#output_media').hide();
}


//Hide post
function hidepost(caller) {
	var id = $(caller).attr('data-id');
  $('.media-' + id).html('<h5 style="margin-bottom: 10px; color: #757575; text-align: center;">Đã ẩn bài đăng</h5>');
}

//Report port
function reportpost(caller) {
  var id = $(caller).attr('data-id');
	$('#myReport').attr('data-id', id).modal('show');
}

function submitreportpost() {
  var id = $('#myReport').attr('data-id');
	var dataString = 'id=' + id;

  $('#myReport').modal('hide');
	//ajax
	$.ajax({
		type: "POST",
		url: "reports.php",
		data: dataString,
		cache: false,
		success: function()
		{
			$('.reportbtn-' + id).html('<a><i class="fas fa-shield-virus" style="font-size: 18px;"></i>&nbsp; Đã báo cáo!</a>');
			$('.reportbtn-' + id).addClass("disabled");
		}
	});
}


//Delete post
function deletepost(caller) {
	var id = $(caller).attr('data-id');
	$('#myDelete').attr('data-id', id).modal('show');
}

function submitdeletepost() {
	var id = $('#myDelete').attr('data-id');
	var dataString = 'id=' + id;
    //$('[data-id=' + id + ']').remove();
    $('#myDelete').modal('hide');
	//ajax
	$.ajax({
    type: "POST",
    url: "user_delete_post.php",
    data: dataString,
    cache: false,
    success: function(html)
    {
	    $('.post-' + id).html(html);
    }
  });
//ajax ends
}


//Edit post
function editpost(caller) {
	var id = $(caller).attr('data-id');
  var dataString = 'id=' + id;
    
	//ajax
	$.ajax({
  type: "POST",
  url: "user_edit_post.php",
  data: dataString,
  cache: false,
  success: function(data)
  {
	  $('#media_detail').html(data);
    $('#modelEditPost').modal("show");
  }
  });
  //ajax ends
}


//Vote functions
function voteup(caller){

  display = $(caller).siblings('span.display-vote');
  votedownbtn = $(caller).siblings('i.fa-thumbs-down');
  var id = $(caller).attr('data-id');
  var p = parseInt(display.attr('data-points'));

  if ($(caller).hasClass('fas')) {
    //action = 'unlike';
    $(caller).removeClass('fas');
    $(caller).addClass('far');
    p = p-1;
    display.attr('data-points', p);
    if (p < 1000) { display.html(p); }
  } else if ( ($(caller).hasClass('far')) && votedownbtn.hasClass('fas') ) {
    //action = 'changetolike';
    $(caller).removeClass('far');
    $(caller).addClass('fas');
    votedownbtn.removeClass('fas');
    votedownbtn.addClass('far');
    p = p+2;
    display.attr('data-points', p);
    if (p < 1000) { display.html(p); }
  } else {
    //action = 'like';
    $(caller).removeClass('far');
    $(caller).addClass('fas');
    p = p+1;
    display.attr('data-points', p);
    if (p < 1000) { display.html(p); }    
  }

  var dataString = { id:id, p:p };

  $.ajax({
    type: "POST",
		url: "vote_up.php",
		data: dataString,
		cache: false,
          success: function(html){              
              if ( p > 999 ) {
                display.html(html);
              }
          }
  });		
}


function votedown(caller){

  display = $(caller).siblings('span.display-vote');
  voteupbtn = $(caller).siblings('i.fa-thumbs-up');
  var id = $(caller).attr('data-id');
  var p = parseInt(display.attr('data-points'));  

  if ($(caller).hasClass('fas')) {
    //action = 'undislike';
    $(caller).removeClass('fas');
    $(caller).addClass('far');
    p = p+1;
    display.attr('data-points', p);
    if (p < 1000) { display.html(p); }
  } else if ( ($(caller).hasClass('far')) && voteupbtn.hasClass('fas') ) {
    //action = 'changetodislike';
    $(caller).removeClass('far');
    $(caller).addClass('fas');
    voteupbtn.removeClass('fas');
    voteupbtn.addClass('far');
    p = p-2;
    display.attr('data-points', p);
    if (p < 1000) { display.html(p); }
  } else {
    //action = 'dislike';
    $(caller).removeClass('far');
    $(caller).addClass('fas');
    p = p-1;
    display.attr('data-points', p);
    if (p < 1000) { display.html(p); }
  }

  var dataString = { id:id, p:p };

  $.ajax({
    type: "POST",
		url: "vote_down.php",
		data: dataString,
		cache: false,
          success: function(html){              
              if ( p > 999 ) {
                display.html(html);
              }
          }
  });		
}

//Search button
function searchbtn() {
  $('#search-box').show();
  $('#term').focus();
}


$(document).ready(function(){

$.getScript('js/ready_functions.js');

/* Auto hide menu dropdown when click outside */
$(document).click(function (event) {
  var clickover = $(event.target);
  var $navbar = $(".navbar-collapse");              
  var _opened = $navbar.hasClass("in");
  if (_opened === true && !clickover.hasClass("navbar-toggle")) {      
    $navbar.collapse('hide');
    $('.menubtn').removeClass('active');
  }
});


//Add action for menu button
$('.menubtn').click(function(){
  $('.menubtn').toggleClass('active');
});

//Upload media photo
$('#mFile').on('change', function(){

    var reader = new FileReader();
    reader.onload = function(e)
    {
        $('#output_media').attr('src', e.target.result);
    }
    reader.readAsDataURL(event.target.files[0]);
    $('#output_media').show();

});


//Input text auto resize height
$('textarea').each(function () {
  this.setAttribute('style', 'height:' + (this.scrollHeight) + 'px;overflow-y:hidden;');
}).on('input', function () {
  this.style.height = 'auto';
  this.style.height = (this.scrollHeight) + 'px';
});


//Show edit post content
$("#modelEditPost").on('shown.bs.modal', function(){
        var content = $(this).find('#inputTitle').attr('data-id');
        $(this).find('#inputTitle').val('');
        $(this).find('#inputTitle').height(0);
        $(this).find('#inputTitle').focus();
        $(this).find('#inputTitle').val(content);
        $(this).find('#inputTitle').each(function () {
		        this.setAttribute('style', 'height:' + (this.scrollHeight) + 'px;overflow-y:hidden;');
  	        }).on('input', function () {
		        this.style.height = 'auto';
		        this.style.height = (this.scrollHeight) + 'px';
            });
});


}); //ready(function())


/* Login */

$(document).ready(function()
{
    $('#FromLogin').on('submit', function(e)
    {
        e.preventDefault();
        $('.submitButton').attr('disabled', ''); //disable login button when processing
        //show uploading message
        $("#output-login").html('<div class="alert alert-info">Đang đăng nhập</div>');
        $(this).ajaxSubmit({
        target: '#output-login',
        success: afterSuccess //call function after success
        });
    });

    //Href to login modal
    if(window.location.href.indexOf('#dangnhap') != -1) {
      $('#myModal').modal('show');
    }
});

//Hide Login Modal if Register Show
$(document).ready(function(){
  $("#registerUser").click(function(){
    $('#myModal').modal('hide');
  });
});

/* Register */

$(document).ready(function()
{
    $('#FromRegister').on('submit', function(e)
    {
        e.preventDefault();
        $('.submitButton').attr('disabled', ''); //disable register button when processing
        //show uploading message
        $("#output-register").html('<div class="alert alert-info">Đang đăng ký</div>'); 
        $(this).ajaxSubmit({
        target: '#output-register',
        success: afterSuccess //call function after success
        });
    });
});


/* Activate */

$(document).ready(function()
{
    $('#FromActivate').on('submit', function(e)
    {
        e.preventDefault();
        $('.submitButton').attr('disabled', ''); //disable activate & resend button when processing
        //show uploading message
        $("#output-activate").html('<div class="alert alert-info">Đang kích hoạt</div>'); 
        $(this).ajaxSubmit({
        target: '#output-activate',
        success: afterSuccess //call function after success
        });
    });
});


/* Resend Activate code */

$(document).ready(function()
{
    $('#FromResend').on('submit', function(e)
    {
        e.preventDefault();
        //$('#FromResend').hide()
        $('#resendButton').attr('disabled', '');
        //show uploading message
        $("#output-activate").html('<div class="alert alert-info">Đang xử lý ...</div>'); 
        $(this).ajaxSubmit({
        target: '#output-activate',
        success: countdownsend
        });
    });
});

//countdown function after resend button clicked
function countdownsend(){

// Set timer countdown (seconds)
var counter = 30;
var interval = setInterval(function() {
    counter--;
    // Display 'counter' wherever you want to display it.
    if (counter <= 0) {
     		clearInterval(interval);
        $('#ctimer').html("");
        $('#resendButton').removeAttr('disabled'); //enable send button
        return;
    }else{
    	$('#ctimer').text("(" + counter + "s)");
      
    }
}, 1000);
}


/* Submit Images */

$(document).ready(function()
{
    $('#FileUploader').on('submit', function(e)
    {
        e.preventDefault();
        $('.submitButton').attr('disabled', ''); // disable upload button
        //show uploading message
        $("#output-image").html('<div class="alert alert-info">Đang tải lên ...</div>');
        
        $(this).ajaxSubmit({
        target: '#output-image',
        success:  afterSuccess //call function after success
        });
    });
});


/* Submit Videos */

$(document).ready(function()
{
    $('#VideoSubmit').on('submit', function(e)
    {
      e.preventDefault();
        $('.submitButton').attr('disabled', ''); // disable upload button
        //show uploading message
        $("#output-video").html('<div class="alert alert-info">Đang tải lên ...</div>');
        
        $(this).ajaxSubmit({
        target: '#output-video',
        success:  afterSuccess //call function after success
        });
    });
});
 
function afterSuccess()
{
  setTimeout(function() {
    $('.submitButton').removeAttr('disabled'); //enable submit button
  },1500);
}


//$Ads
var ads_num=8;

$(document).ready(function() {
    var s = $("#ads-two");
    var pos = s.position();                   
    $(window).scroll(function() {
        var windowpos = $(window).scrollTop();
       
        if (windowpos-5 >= pos.top) {
            s.addClass("ads-two");
        } else {
            s.removeClass("ads-two");
        }
    });
});

$(document).ready(function(){

    $('[data-toggle="tooltip"]').tooltip();   

});
//Ads


//Add active class to menu bar to show css
$(function(){
  $('.nav a').filter(function(){
    return this.href==location.href
    }).parent().addClass('active').siblings().removeClass('active');
});


//Auto hide Navbar and Go to top button function
var prevScrollpos = window.pageYOffset;
window.onscroll = function() {
var currentScrollPos = window.pageYOffset;
  if (prevScrollpos < currentScrollPos && window.pageYOffset > 100) {
    $('#myNavbar').css('top', '-100px');
    $('#myBtn').hide();
  } else {
    $('#myNavbar').css('top', '0px');
    $('#myBtn').show();
  }
  prevScrollpos = currentScrollPos;
}


//gán sự kiện click cho go to top button
function scrollWin() {
  $(window.opera ? 'html' : 'html, body').animate({
        scrollTop: 0
    }, 'slow');
}
///Top button


//Error page
function errorpage() {
  window.location = "error.html";
}
setTimeout("leave()", 0);


</script>
<!-- End of js -->


<?php

// Check if user already likes post or not
function userLiked($post_id)
{
  global $mysqli;
  global $Uid;
  $sql = "SELECT id FROM voteip WHERE uid=$Uid AND media_id=$post_id AND type='1'";
  $result = mysqli_query($mysqli, $sql);
  if (mysqli_num_rows($result) > 0) {
  	return true;
  }else{
  	return false;
  }
}

// Check if user already dislikes post or not
function userDisliked($post_id)
{
  global $mysqli;
  global $Uid;
  $sql = "SELECT id FROM voteip WHERE uid=$Uid AND media_id=$post_id AND type='2'";
  $result = mysqli_query($mysqli, $sql);
  if (mysqli_num_rows($result) > 0) {
  	return true;
  }else{
  	return false;
  }
}


// Check if user already reported post or not
function userReported($post_id)
{
  global $mysqli;
  global $Uid;
  $sql = "SELECT id FROM reports WHERE uid=$Uid AND media_id=$post_id";
  $result = mysqli_query($mysqli, $sql);
  if (mysqli_num_rows($result) > 0) {
  	return true;
  }else{
  	return false;
  }
}


// Check if number post limited
function checkNumPosts() {
  global $mysqli;
  global $Uid, $LimitPost, $Ubonus;
  $BeginToday = strtotime((new DateTime('today', new DateTimeZone('Asia/Ho_Chi_Minh')))->format('c'));
	$MediaSql = "SELECT id FROM media WHERE uid='$Uid' AND UNIX_TIMESTAMP(date)>'$BeginToday'";
	$MediaResult = mysqli_query($mysqli, $MediaSql);
	$MediaCount = mysqli_num_rows($MediaResult);
  if ($MediaCount < $LimitPost) {
    $numPost = $LimitPost - $MediaCount;
  } else {
    $numPost = 0;
  }
  $maxpost = $numPost + $Ubonus;
  return $maxpost;
}


?>



<!-- </head> -->
<!-- end -->