<?php session_start();
include('header_php.php');

//Get  Profile Info

$pid = $mysqli->escape_string($_GET['id']);

if($Profile = $mysqli->query("SELECT * FROM users WHERE uid='$pid'")){

  $ProfileRow = mysqli_fetch_array($Profile);
	
	$ProfileImage = $ProfileRow['avatar'];

	$ProfilelUname = $ProfileRow['username'];
	
	$ProfileUsername = convertvn($ProfilelUname);

	$UserPoints = $ProfileRow['points'];
		
	$Profile->close();
	
}else{
	?><script>errorpage();</script><?php
}

?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title><?php echo $ProfileRow['username']." - ".$Settings['name']; ?></title>
<meta name="description" content="<?php echo $Settings['descrp']; ?>" />
<meta name="keywords" content="<?php echo $Settings['keywords']; ?>" />

<!--Facebook Meta Tags-->
<meta property="fb:app_id"          content="<?php echo $Settings['fbapp']; ?>" />
<meta property="og:url"             content="<?php echo $Settings['siteurl']; ?>/profile-<?php echo $ProfileRow['uid'];?>-<?php echo $ProfileUsername;?>.html" />
<meta property="og:title"           content="<?php echo $ProfileRow['username']; ?>" />
<meta property="og:description" 	  content="<?php echo $Settings['descrp']; ?>" />

<?php	if (empty($ProfileImage)) { 
	$Avatar = $Settings['datalink']."/sysimg/default-avatar.png";
} elseif (substr($ProfileImage,0,4) == "http"){
  $Avatar = $ProfileImage;
} else {
  $Avatar = $Settings['datalink'].'/avatars/'.$ProfileImage;
} ?>

<meta property="og:image"           content="<?php echo $Avatar; ?>" />
<!--End Facebook Meta Tags-->

<style>
/*Profile Page */
.profile-box {
  margin: 20px 10px 10px 10px;
}
.img-profile{
	float: left;
	width:100px;
	height:100px;
}
.show-description {
	height: 100px;
	margin-left: 110px;
}
.profile-user-name{ 
	padding-top: 5px;
	padding-bottom: 15px;
}
.profile-description{ 
	font-size:14px;
	color: #777;
}
.alert_no_post {
  margin: 10px 10px 10px 10px;
  color: #656565;
}

</style>


<!--- Require php file --->
<?php 
include('header_js.php');
?>


</head>


<!--- Require php file --->
<?php 
include('header_div.php');
?>

<!-- Title line -->
<div class="bottom-header">
  <div class="container">
    <div class="header-bottom-left">
      <div id="slogan"><?php echo $Settings['descrp']; ?></div>
      <div id="social-love">
        <div id="fb-button-div">
          <div class="fb-like" data-href="<?php echo $Settings['fbpage'];?>" data-width="50px" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>
        </div>
      </div>
      <!--/.social-love -->     
    </div>
    <!--/.header-bottom-left -->

    <div id="search-box">
      <form class="navbar-form" role="search" id="search" method="get" action="search.php">
        <div class="input-group">
          <input type="text" class="form-control" placeholder="Tìm kiếm" id="term" name="term">
          <div class="input-group-btn">
            <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
          </div>
        </div>
      </form>
    </div>
  </div>
  <!--center-header--> 
</div>
<!--bottom-header-->

<div class="container">
	<div class="show-profile">
    <div class="profile-box">
    	<div class="img-profile">
        <img class="avatar-box" src="<?php echo $Avatar; ?>" alt="avatar" />
		  </div> <!-- /.img-profile -->
		
		  <div class="show-description">
			  <div class="profile-user-name"><h2 style="word-wrap:break-word;"><?php echo $ProfilelUname; ?></h2></div><!--profile-user-name--> 		
  			<div class="profile-description"><?php echo number_format($UserPoints, 0, ',', '.');?>&nbsp;lượt thích</div><!--profile-user-name-->
        
        <?php if ($pid == $Uid) { ?>
          <div class="profile-description">Còn <span id="maxpost"><?php echo number_format(checkNumPosts(), 0, ',', '.'); ?></span>&nbsp;lượt đăng bài</div>
        <?php } ?>
      </div>

    </div>
	
	</div><!--show-profile-->
  
</div><!-- /.container --> 

<!-- end -->