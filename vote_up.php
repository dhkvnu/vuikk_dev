<?php session_start();

include('../db.php');

if (!isset($_SESSION['useremail'])){

	$Uid = 0;

} else {
	
	$uEmail = $_SESSION['useremail'];

	if($UserSql = $mysqli->query("SELECT * FROM users WHERE email='$uEmail' limit 1")){

    	$UserRow = mysqli_fetch_array($UserSql);

		$Uid = $UserRow['uid'];
	
		$UserSql->close();
	
	} else {
    	?><script>errorpage();</script><?php	 
	}
}


if (isset($_POST['id']))
{

$id = $mysqli->real_escape_string($_POST['id']);
$p = $mysqli->real_escape_string($_POST['p']);

$CheckIp = $mysqli->query("SELECT * FROM voteip WHERE media_id='$id' AND uid='$Uid' limit 1");
$VoteType = mysqli_fetch_array($CheckIp);
$Vote = $VoteType['type'];
$Count = mysqli_num_rows($CheckIp);
$CheckIp->close();

$CheckPost = $mysqli->query("SELECT * FROM media WHERE active=1 AND id='$id' limit 1");
$PostRow = mysqli_fetch_array($CheckPost);
$PostedById = $PostRow['uid'];
$CountMedia = mysqli_num_rows($CheckPost);
$CheckPost->close();

//Check if post exist
if ($CountMedia != 0) {

//First click
if ($Count == 0)
{
	$mysqli->query("UPDATE media SET votes=votes+1,views=views+1 WHERE id='$id'");
	$mysqli->query("UPDATE users SET points=points+1 WHERE uid='$PostedById'");
	$mysqli->query("INSERT INTO voteip (media_id,uid,type) VALUES ('$id','$Uid','1')");

} else {
//Check vote up or down

switch ($Vote) {

//Up Clicked
case 1: 
	$mysqli->query("UPDATE media SET votes=votes-1 WHERE id='$id'");
	$mysqli->query("UPDATE users SET points=points-1 WHERE uid='$PostedById'");
	$mysqli->query("UPDATE voteip SET type=0 WHERE media_id='$id' AND uid='$Uid'");
	break;

//Down clicked	
case 2:
	$mysqli->query("UPDATE media SET votes=votes+2 WHERE id='$id'");
	$mysqli->query("UPDATE users SET points=points+2 WHERE uid='$PostedById'");
	$mysqli->query("UPDATE voteip SET type='1' WHERE media_id='$id' AND uid='$Uid'");
	break;

case 0:
	$mysqli->query("UPDATE media SET votes=votes+1 WHERE id='$id'");
	$mysqli->query("UPDATE users SET points=points+1 WHERE uid='$PostedById'");
	$mysqli->query("UPDATE voteip SET type='1' WHERE media_id='$id' AND uid='$Uid'");
	break;

default:
	break;
	}
}
// End vote up

	$result=$mysqli->query("SELECT votes FROM media WHERE id='$id'");
	$row=mysqli_fetch_array($result);
	$Points = $row['votes'];
	$result->close();

	if($Points > 100 && $PostRow['bonus_check'] == 0) {
		$mysqli->query("UPDATE media SET bonus_check=1 WHERE id='$id'");
		$mysqli->query("UPDATE users SET bonus=bonus+1 WHERE uid='$PostedById'");
		
		//Send bonus alert mail
		include('send_alert_bonus.php');
	}

} else {
	$Points = (int)$p; //Return p if post deleted
}


//Function to convert Vote Points
function show_point($input)
{
	if ( is_numeric($input) ) {	
    $suffixes = array('', 'N', 'Tr', 'T', 'NT');
    $suffixIndex = 0;

    while(abs($input) >= 1000 && $suffixIndex < sizeof($suffixes))
    {
        $suffixIndex++;
        $input /= 1000;
        $input = round($input,1);
    }

    return str_replace('.',',', (
        $input > 0
            // precision of 3 decimal places
            ? floor($input * 1000) / 1000
            : ceil($input * 1000) / 1000
        ))
		. $suffixes[$suffixIndex];
	} else {
		return "-";
	}
}

echo show_point($Points);

}
?>