<?php session_start();

//initialize facebook sdk
require 'facebook/vendor/autoload.php';

include('../db.php');


if($site_settings = $mysqli->query("SELECT * FROM settings WHERE id='1'")){

  $Settings 	= mysqli_fetch_array($site_settings);
	
	$fb_app 	= stripslashes($Settings['fbapp']);
	
	$fb_key 	= stripslashes($Settings['fb_key']);

  $site_settings->close();
	
}else{
    
	?><script>errorpage();</script><?php
}


$fb = new Facebook\Facebook([

  'app_id' => $fb_app,
 
  'app_secret' => $fb_key,
 
  'default_graph_version' => 'v2.7',
 
 ]);

 $helper = $fb->getRedirectLoginHelper();

 $permissions = ['email']; // Optional to get email

 try {

  if (isset($_SESSION['facebook_access_token'])) {
  
  $accessToken = $_SESSION['facebook_access_token'];
  
  } else {
  
    $accessToken = $helper->getAccessToken();
  
  }
  
  } catch(Facebook\Exceptions\facebookResponseException $e) {
  
  // When Graph returns an error
  
  echo 'Graph returned an error: ' . $e->getMessage();
  
    exit;
  
  } catch(Facebook\Exceptions\FacebookSDKException $e) {
  
  // When validation fails or other local issues
  
  echo 'Facebook SDK returned an error: ' . $e->getMessage();
  
    exit;
  
  }

  if (isset($accessToken)) {

    if (isset($_SESSION['facebook_access_token'])) {
    
    $fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
    
    } else {
    
    // getting short-lived access token
    
    $_SESSION['facebook_access_token'] = (string) $accessToken;
    
      // OAuth 2.0 client handler
    
    $oAuth2Client = $fb->getOAuth2Client();
    
    // Exchanges a short-lived access token for a long-lived one
    
    $longLivedAccessToken = $oAuth2Client->getLongLivedAccessToken($_SESSION['facebook_access_token']);
    
    $_SESSION['facebook_access_token'] = (string) $longLivedAccessToken;
    
    // setting default access token to be used in script
    
    $fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
    
    }


// getting basic info about user

try {

$profile_request = $fb->get('/me?fields=name,first_name,last_name,email');

$requestPicture = $fb->get('/me/picture?redirect=false&height=200'); //getting user picture

$picture = $requestPicture->getGraphUser();

$profile = $profile_request->getGraphUser();

// Save the user nformation in session variable
$_SESSION['snid'] = $snid = $profile['id'];           // To Get Facebook ID

$_SESSION['sn_name'] = $profile['name'];   // To Get Facebook full name

$_SESSION['sn_email'] = $profile['email'];    //  To Get Facebook email

$_SESSION['sn_pic'] = $picture['url'];    //  To Get Facebook avatar

$_SESSION['provider'] = "FB";  // Social Network Name

// </try>
} catch(Facebook\Exceptions\FacebookResponseException $e) {

// When Graph returns an error

echo 'Graph returned an error: ' . $e->getMessage();

session_destroy();

// Redirecting user back to app login page
header("Location: .");

exit;

} catch(Facebook\Exceptions\FacebookSDKException $e) {

// When validation fails or other local issues
echo 'Facebook SDK returned an error: ' . $e->getMessage();

exit;
}

// Check user exited

if($UserCheck = $mysqli->query("SELECT * FROM users WHERE snid='$snid'")){
	
  $get_user = mysqli_fetch_array($UserCheck);

  $get_useremail = $get_user['email'];

  $VdUser = $UserCheck->num_rows;

  $UserCheck->close();
 
}else{
 
  ?>
	<script>
		errorpage();
	</script>
	<?php

}
  
/* header location after session */

if ($VdUser == 0 || empty($get_useremail)){

header("Location: register_sn.html");	

} 

else{

$_SESSION['useremail'] = $get_useremail;  //use email session to login

header("Location: .");

//Save to cookie
$cookie_expiration_time = time() + (86400 * 365);  // for 1 year
setcookie ("useremail", $get_useremail, $cookie_expiration_time);
setcookie ("snid", $snid, $cookie_expiration_time);

//unset old session
unset($_SESSION['snid']);
unset($_SESSION['sn_name']);
unset($_SESSION['sn_pic']);
unset($_SESSION['provider']);

  }
} 
//end accessToken>

else{           
  $loginUrl = $helper->getLoginUrl($Settings['siteurl'].'/config_fb.php', $permissions);
  header("Location: ".$loginUrl);
  
  }

?>